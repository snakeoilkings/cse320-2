#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <wait.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <netdb.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <openssl/sha.h>
#include <ctype.h>
#include <openssl/rand.h>

#define TRUE 1
#define FALSE 0

/* Assume no input line will be longer than 1000 bytes */
#define MAX_INPUT 1001

/* Assume max amount of users as 100 for now. Change later */
#define MAX_USERS FD_SETSIZE

typedef char boolean; 
typedef struct sockaddr SA;

typedef struct room {
	char name[MAX_INPUT];
	int id;
	int owner;
	int total_users;
	int type;
	char password[MAX_INPUT];
} room;

typedef struct user {
	int id;					/* might use later if it makes programming easier */
	int state;				/* reflects where the user is */
	int fd;					/* user's file descriptor for connection to server */
	char name[MAX_INPUT];   /* user-defined name */
} user;

typedef struct pool {
	fd_set read_set;	/* set of all active file descriptors */
	fd_set ready_set ;	/* subset of descriptors ready with input */
	int total;		    /* number of users online */
	int maxi;			/* highwater index into client array */
	int maxfd;			/* max file descriptor */
	int clientfd[FD_SETSIZE];  /* set of all active file descriptors */
	user user_list[MAX_USERS]; /* list of all users */
} pool;

/* global vars */
int listenfd;					/* listening fd */
int port; 				 		/* the listening port */
char motd[MAX_INPUT];  		    /* motd */
int max_rooms = 5;	      		/* max number of rooms */
pool user_pool;				    /* pool of users */
room room_list[100000];		    /* initialize the room list */	
int room_total=0;		


/* run-time flags */
boolean echo_flag = FALSE;		/* echo on server or not */
boolean room_flag = FALSE;		/* num rooms identified */
boolean room_exists = FALSE;	/* is there an echo thread running */
 
/* methods */

void sig_handler(int sig);
int open_listen_fd(char* port);
void* echo_thread(void* vargp);
void* login_thread(void* vargp);
void add_client(int connfd, char* user_id);
void init_pool();
void init_rooms();
int check_name(char* id);
int remove_username(int id);
int echo(char* msg, int room);
int create_room(char* name, int type, char* pw);
ssize_t send(int sockfd, const void *buf, size_t len, int flags);

void printerr(int errnum, int fd, boolean kill);
int validate_user(char* name);
int validate_password(char* pw);
int save_user(char* id, char* pw); 
char* find_name(char* id);
boolean user_authentication(char* e_pw, char* pw);
void replace_owner(int room_id, int i);


int main (int argc, char ** argv, char **envp) {
	
	init_rooms();
  
    int port_arg, message_arg, connfd;
    char client_hostname[MAX_INPUT], client_port[MAX_INPUT];
    struct sockaddr_storage clientaddr;
    socklen_t clientlen;
    pthread_t tid;
		
    /* let's sig_int handle a SIGINT call */
    signal (SIGINT, sig_handler);
	
	/* block sigpipe and handle epipe instead */
	signal (SIGPIPE, SIG_IGN);
        
    /* looks for optional flags and responds accordingly */
    extern int optind;
    int opt;
    while((opt = getopt(argc, argv, "ehN")) != -1) {
        switch(opt) {
            case 'e':
                     /* set echo flag for program */
                        echo_flag = TRUE;
                        break;
            case 'h':
                     /* print out help menu and exit */
                        //printf("\x1B[2J");
						printf("USAGE:\n\n./server [-heN] PORT_NUMBER MOTD\n\n-e\t\tEcho messages received on server's stdout.\n-h\t\tDisplays help menu & returns EXIT_SUCCESS.\n-N num\t\tSpecifies maximum number of chat rooms allowed on server.\n\t\tDefault = 5\nPORT_NUMBER\tPort number to listen on.\nMOTD\t\tMessage to display to the client when they connect.\n");
                        return EXIT_SUCCESS;
			case 'N':
                     /* set maximum room size - add check for 
					  * digit verification later */
						max_rooms = atoi(argv[optind]);
						room_flag = TRUE;
						printf("Number of rooms allowed: %d.\n", max_rooms);
            default:
						break;
        }
    }
		
	/* Move arg counter forward over room size */
	if (optind < argc && (argc - optind) == 3 && room_flag == TRUE) 
			optind++;	
		
    /* Get positional arguments */
    if(optind < argc && (argc - optind) == 2) {
        /* sets port = to first argument */
        port_arg = optind++;
        /* sets message to second argument */	
		message_arg = optind++;
    } 
	else {
    /* handles when amount of arguments aren't what they should be */
            if((argc - optind) <= 0) 
                fprintf(stderr, "Missing PORT and MESSAGE. Exiting...\n");
            else if((argc - optind) == 1) 
                fprintf(stderr, "Too few arguments. Exiting...\n");
            else 
                fprintf(stderr, "Too many arguments provided. Exiting...\n");
        exit(EXIT_FAILURE);
    }
    
    /* put port number in port; motd in message */
    port = atoi(argv[port_arg]);
    strcpy(motd, argv[message_arg]);
        
    /* check for valid port number, then start listening */
    if (port > 1023 && port <= 65535) {
        listenfd = open_listen_fd(argv[port_arg]);
		if (listenfd < 0) {
			printf("Error establishing a socket with the kernel. Server may already be running on this port or being used by another program. Exiting...\n");
			exit(EXIT_FAILURE);
		}
        printf("Currently listening on port %d.\n", port);
    }
    else
        /* port number invalid, need to exit and try a new port number */
        return EXIT_FAILURE;
            
    while(1) {
            clientlen = sizeof(struct sockaddr_storage);
            /* get a file descriptor for client connection */
            connfd = accept(listenfd, (SA*)&clientaddr, &clientlen);
            /* place connfd into a shared spot */
            void* conn = malloc(sizeof(int));
            *((int*)conn) = connfd;
            /* get client info */
            getnameinfo((SA*) &clientaddr, clientlen, client_hostname, MAX_INPUT, 
			  client_port,  MAX_INPUT, 0);
            printf("Connected to %s, at port %s\n", client_hostname, client_port);
            /* create thread */
            pthread_create(&tid, NULL, login_thread, conn);
    }
    
    return EXIT_SUCCESS;
}

/** process ending handler closes all fds before exit **/
void sig_handler(int sig) {
        int i;
		
		/* close all ports */
		for (i = 0; i <= user_pool.maxi; i++) 
			if (user_pool.clientfd[i] >= 0) {
				send(user_pool.clientfd[i], "ERR 100 Internal Server Error. \r\n", 33, 0);
				close(user_pool.clientfd[i]);
			}
		
		/* close listening port */
        close(listenfd);
		
		/* exit the program */
        exit(0);
        
}

/** creates listening port **/
int open_listen_fd(char* port) {
        struct addrinfo hints, *listp, *p;
        int listenfd, optval = 1;
        
        /* gets list of potential server addresses */
        memset(&hints, 0 , sizeof(struct addrinfo));
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE | AI_ADDRCONFIG;
        hints.ai_flags |= AI_NUMERICSERV;
        getaddrinfo(NULL, port, &hints, &listp);
        
        /* walk list for one we can bind to */
        for (p = listp; p; p = p-> ai_next) {
            /* create socket descriptor */
            if ((listenfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) 
                continue;
                        
             /* eliminate address already in use error from bind */
              setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval, sizeof(int));
                        
             /* bind descriptor to this address */
             if (bind(listenfd, p->ai_addr, p->ai_addrlen) == 0) 
				break; /* success */
				
            close(listenfd); /* bind failed, try again */
        }
        /* clean up */
        freeaddrinfo(listp);
        if (!p) /* no address found */ {
            return -1;
		}    
        /* make it a listening socket ready to accept connection requests */
        if (listen(listenfd, MAX_INPUT) < 0) {
            close(listenfd);
            return -1;
        }
        return listenfd;
}

/** login thread **/
void* login_thread(void* vargp) {
	
	int r, is_valid;
	char command[MAX_INPUT];
	pthread_t tid;
	
    /* grab connection file descriptor from thread */
    int connfd = *((int*)vargp);
	
    /* detach into own thread so main thread needn't has to wait */
    pthread_detach(pthread_self());
	
    /* free the malloced space */
    free(vargp);
	
	/* read verb */
    r = recv(connfd , command, MAX_INPUT , 0);
	if (r <= 0) {
		close(connfd);
		return NULL;
	}
	
	char* verb = strtok(command, " ");
	
	/* check if login verb */
	if (!strcmp(verb, "ALOHA!")) 
		send(connfd, "!AHOLA \r\n", 9, 0); 
	
	else {
		send(connfd, "ERR 100 Internal server error. \r\n", 33, 0);
		close(connfd);
		return NULL;
	}
	
	/* read verb */
    r = recv(connfd , command, MAX_INPUT , 0);
	if (r <= 0) {
		close(connfd);
		return NULL;
	}
	
	/* parse command */
	verb = strtok(command, " ");
	
	char hi[MAX_INPUT];
	char user_id[MAX_INPUT];
	bzero(user_id, MAX_INPUT);
	
	/* are we logging in? */
	if (!strcmp(verb, "IAM")) {
		char* buf1 = strtok(command+4, " ");
		strcpy(user_id, buf1);
		
		/* check if username is already on server */
		is_valid = check_name(user_id);
		
		/* check if name actually exists */
		char* pw = find_name(user_id);
		
		if (!is_valid && pw != NULL) {
			
			/* name exists. proceed */
			
			/* prompt client you want authorization */
			sprintf(hi, "AUTH %s \r\n", user_id);
			send(connfd, hi, strlen(hi), 0);
			
			/* read password */
			bzero(command, MAX_INPUT);
			
			r = recv(connfd , command, MAX_INPUT, 0);
			if (r <= 0) {
				close(connfd);
				return NULL;
			}
			
			/* parse command */
			verb = strtok(command, " ");
			
			if (!strcmp(verb, "PASS")) {
				char* user_pw = strtok(NULL, " ");
				bzero(hi, MAX_INPUT);
				
				/* check if pw is correct */
				boolean auth = FALSE;
				
				auth = user_authentication(user_pw, pw);
				
				/* if so, complete handshake */
				if (auth == TRUE) {
					sprintf(hi, "HI %s \r\n", user_id);
					send(connfd, hi, strlen(hi), 0);
				}
				/* otherwise authentication failed. disconnect */
				else {
					sprintf(hi, "ERR 61 Password not valid. \r\n");
					
					/* send error msg & kill thread*/
					send(connfd, hi, strlen(hi), 0);
					close(connfd);
					return NULL;
				}
			}
			else {
				/* send error msg & kill thread*/
				send(connfd, "ERR 100 Internal server error. \r\n", 33, 0);
				close(connfd);
				return NULL;
			}
		}
		else {
			/* name already exists */
			bzero(hi, MAX_INPUT);
			sprintf(hi, "ERR 00 SORRY %s \r\n", user_id);
			
			/* send error msg & kill thread*/
			send(connfd, hi, strlen(hi), 0);
			close(connfd);
			return NULL;
		}
	}
	
	/* or are they making a new user? */
	else if (!strcmp(verb, "IAMNEW")) {
		char* buf2 = strtok(command+7, " ");
		bzero(user_id, MAX_INPUT);
		strcpy(user_id, buf2);

		/* check if username exists already */
		char* valid = find_name(user_id);
	
		if (valid==NULL) {
			bzero(hi, MAX_INPUT);
			sprintf(hi, "HINEW %s \r\n", user_id);
			send(connfd, hi, strlen(hi), 0);
		
			/* read password */
			bzero(command, MAX_INPUT);
			r = recv(connfd , command, MAX_INPUT , 0);
			if (r <= 0) {
				close(connfd);
				return NULL;
			}
			/* parse it */
			verb = strtok(command, " ");
			
		
			if (!strcmp(verb, "NEWPASS")) {
				
				char* user_pw = strtok(command+8, " ");
	
				int valid_pw = validate_password(user_pw);

				if (valid_pw == 0)  {
					
					int o = save_user(user_id, user_pw);
					if (o < 0) {
						/* send error msg & kill thread*/
						send(connfd, "ERR 100 Internal server error. \r\n", 33, 0);
						close(connfd);
						return NULL;
					}
					bzero(hi, MAX_INPUT);
					sprintf(hi, "HI %s \r\n", user_id);
					send(connfd, hi, strlen(hi), 0);
				}
				else {
					send(connfd, "ERR 61 Invalid Password. \r\n", 27, 0);
					close(connfd);
					return NULL;
				}
			}
			else {
					/* send error msg & kill thread*/
					send(connfd, "ERR 100 Internal server error. \r\n", 33, 0);
					close(connfd);
					return NULL;
				}
		}
		else {
			/* name taken, send error */
			char error_msg[MAX_INPUT];
			bzero(error_msg, MAX_INPUT);
			sprintf(error_msg, "ERR 00 SORRY %s \r\n", user_id);
			send(connfd, error_msg, strlen(error_msg), 0);
			
			close(connfd);
			return NULL;
		}
	}

	else {
		send(connfd, "ERR 100 Internal server error. \r\n", 33, 0);
		close(connfd);
		return NULL;
	}

	/* user name is ok, handshake is completed successfully. */
				
	/* send message of the day out */
	char msg[MAX_INPUT];
	sprintf(msg, "ECHO server %s \r\n", motd);
	send(connfd, msg, strlen(msg), 0);
		
	if (room_exists == TRUE) 
		/* add user into multiplexing pool */
		add_client(connfd, user_id);	
	else {
		/* initialize pool, add new user, and create echo thread */
		init_pool();
		add_client(connfd, user_id);
		pthread_create(&tid, NULL, echo_thread, &user_pool);
		room_exists = TRUE;
	}
	return NULL;
}



/** single thread for multiple users to chat together with **/
void* echo_thread(void* vargp) {
	int r, i, j, n, connfd;
	
	/* detach into own thread so main thread needn't has to wait */
    pthread_detach(pthread_self());
	
	char* chat_str = "ECHO server \n******************************** \n*        Waiting Room          * \n******************************** \r\n";
	char input[MAX_INPUT+4];
	char output[MAX_INPUT+12];
	char* tok;
	char message[MAX_INPUT];
	boolean skip = FALSE;
	
	/* set up a timeval struct for proper time out */
		struct timeval timeout = {0,10};

	while(1) {		
	
		bzero(output, MAX_INPUT+12);
		bzero(message, MAX_INPUT);
		
		/* reset the read set */
		user_pool.ready_set = user_pool.read_set;
		
		/* wait for people to type into chat */
		if ((n = select(user_pool.maxfd+1, &user_pool.ready_set, NULL, NULL, &timeout)) < 0)
			/* PUT ERROR HANDLING HERE */
			perror("select");
		
		/* check for users within a waiting room */
		for (i = 0; i <= user_pool.maxi; i++) {
			connfd = user_pool.clientfd[i];
			if (connfd > 0 && user_pool.user_list[i].state == 0) {
				
				send(connfd, chat_str, strlen(chat_str), 0);
				user_pool.user_list[i].state = 1;
				
				/* no rooms available. Prompt user to make one */
				if (room_total==0) {		
					bzero(output, MAX_INPUT);				
					sprintf(output, "ECHO server There are currently no rooms.\n\nWould you like to create one? \r\n");
					send(connfd, output, strlen(output), 0);
				}
			}
		}

		/* echo to each user */
		for (i = 0; i <= user_pool.maxi && n > 0; i++) {
			
			connfd = user_pool.clientfd[i];
			
			if (connfd > 0 && FD_ISSET(connfd, &user_pool.ready_set)) {
									
				n--;
				bzero(input, MAX_INPUT + 4);
				skip = FALSE;
				
				/* grab what they wrote */
				if ((r = recv(connfd, input, MAX_INPUT , 0)) > 0) {
				
					if (r <= 0) {
						close(connfd);
						skip = TRUE;
						tok = NULL;
					}
					
					if (skip == FALSE)
						/* clear verb */
						tok = strtok(input, " ");

					
					if (!strcmp(tok, "BYE")) {
						/* user is leaving, close connection */
						close(connfd);
						
						/* echo confirmed BYE back to user */
						send(connfd, "BYE \r\n", 6, 0);
						
						/* send echo to all users that this client left */
						sprintf(output, "ECHO server %s has disconnected.", user_pool.user_list[i].name);
						
						if (echo_flag == TRUE) 
							printf("\x1B[1;34mserver > %s has disconnected.\x1B[0m\r\n", user_pool.user_list[i].name);
							
						if (user_pool.user_list[i].state > 1){
							echo(output, user_pool.user_list[i].state);
							room_list[user_pool.user_list[i].state-2].total_users--;
							
							/* get room id */
							int room_id = user_pool.user_list[i].state - 2;
							user_pool.user_list[i].state = 0;
						
							/* see if leaving user is room owner */
							if (room_list[room_id].owner == i) 
								replace_owner(room_id, i);								
						}
						
						/* remove username from shared list */
						remove_username(i);
						
						if (user_pool.total < 1) {
							room_exists = FALSE;
							return NULL;
						}
					}
					
					else if (!strcmp(tok, "MSG")) {
						/* echo message to all users */
						
						/* see if user is in the waiting room */
						if (user_pool.user_list[i].state < 2) {
							sprintf(output, "ERR 60: Not in a room. \r\n");
							send(connfd, output, strlen(output), 0);
						}
						
						else {
						
							/*get username of person who wrote msg */
							char* name = user_pool.user_list[i].name;
						
							/* edit buffer */
							sprintf(output, "ECHO %s", name);
							
							if (echo_flag == TRUE) 
								printf("%s> ", name);
						
							/* parse */
							while (tok != NULL) {
								tok = strtok(NULL, " ");
								if (tok != NULL) {
									if (echo_flag == TRUE) 
										printf("%s ", tok);
									strcat(output, " ");
									strcat(output, tok);
								}
							}
						
							/* echo to users */
							echo(output, user_pool.user_list[i].state);
							
							if (echo_flag == TRUE) 
								printf("\r\n");
						}
					}
					else if (!strcmp(tok, "CREATER")) {
						
						/* check to see if client is already in a room */
						if (user_pool.user_list[i].state > 1) 
							send(connfd, "ERR 60 In a room. \r\n", 24, 0);
						else {				
							bzero(output, MAX_INPUT + 12);
						
							/* parse */
							while (tok != NULL) {
								tok = strtok(NULL, " ");
								if (tok != NULL && strcmp(tok, "\r\n")) {
									strcat(output, " ");
									strcat(output, tok);
								}							
							}
						
							int room_valid;
						
							/* attempt to create a room */
							if (strlen(output) > 2)
								room_valid = create_room(output, 1, NULL);
							else{
								send(connfd, "ERR 60 INVALID OPERATION. \r\n\0", 29, 0);
								room_valid = -3;
							}
							/* send confirmation to client */
							if (room_valid == 0) {
							
								/* add to local counter */
								room_total++;
							
								/* respond to client request */
								sprintf(message, "RETAERC %s \r\n", output);
								send(connfd, message, strlen(message), 0);
							
								/* put user in new room (each room # = index + 2)*/
								user_pool.user_list[i].state = room_valid + 2;
							
								/* set user as room owner */
								room_list[room_valid].owner = i;
							
								/* set total users to 1 */
								room_list[room_valid].total_users = 1;
							
								/* clear message buffer */
								bzero(message, MAX_INPUT);
								
								if (echo_flag == TRUE) 
									printf("\x1B[1;34mserver> %s has connected.\x1B[0m\r\n", user_pool.user_list[i].name);
							
								/* send to all users in the waiting room */
								for (j = 0; j <= user_pool.maxi; j++) {
									connfd = user_pool.clientfd[j];
									if (connfd > 0 && user_pool.user_list[j].state == 1) {
										sprintf(message, "ECHO server New Chat room added. \r\n");
										send(connfd, message, strlen(message), 0);
										if (echo_flag == TRUE) 
											printf("\x1B[1;34mserver> New Chat room added. \x1B[0m\r\n");
									}
								}							
							}	
							else if (room_valid == -1)
									send(connfd, "ERR 10 Room already exists. \r\n\0", 31, 0);
							else if (room_valid == -2)
									send(connfd, "ERR 11 MAXIMUM ROOMS REACHED. \r\n\0", 33, 0);
						}
					}
					else if (!strcmp(tok, "LISTR")) {
						/* print room list */
						
						int current_room = user_pool.user_list[i].state;
						
						/* check to see if user is in waiting room */
						if (current_room > 1 || current_room < 0) 
							send(connfd, "ERR 60 Not in waiting room. \r\n", 30, 0);
						else {
							bzero(output, MAX_INPUT + 12);
							sprintf(output, "RTSIL ");
							
							if (room_total == 0) 
								/* notify user no rooms are available */
								send(connfd, "RTSIL no_rooms -1 \r\n\r\n", 22 ,0);
							else {
								
								for (j=0; j < max_rooms; j++) 
									/* build list */
									if (room_list[j].id >= 0) {
										bzero(message, MAX_INPUT);
										sprintf(message, "%s %d %d \r\n", room_list[j].name, room_list[j].id, room_list[j].type);
										strcat(output, message);
									}
							
								/* finish list. at the very least, user requesting list is on it */
								strcat(output, "\r\n");
							
								/* send list */
								send(connfd, output, strlen(output), 0);
							}
						}
					}
					else if (!strcmp(tok, "JOIN")) {
					
						/* join a room */
						
						/* check to see if user is in room already */
						if (user_pool.user_list[i].state > 1) 
							send(connfd, "ERR 60 Already in a room. \r\n", 28, 0);
						else {
							
							/* parse room id */
							tok = strtok(NULL, " ");
						
							/* see if they actually wrote something for id */
							if (tok == NULL) 
								send(connfd, "ERR 20 Room does not exist. \r\n", 30, 0);
							else {	
								int room_id = -1;
								
								char str[strlen(tok)];
								boolean valid_num = TRUE;
								strcpy(str, tok);
								
								/* see if the client entered a valid number */
								for (j=0; j < strlen(str)-1; j++) {
									if (isdigit(str[j])==0) {
										send(connfd, "ERR 20 Room does not exist. \r\n", 30, 0);
										valid_num = FALSE;
									}
								}
							
								if (valid_num == TRUE) {
									
									/* success! number is valid. Now check if room exists */
									room_id = atoi(str);
							
									/* is the room private? */
									int private = room_list[room_id].type;
									
									/* if a room name has a length of 0, it was never initialized
									 * and doesn't exist */
									if (room_id >= 0 && strlen(room_list[room_id].name)>0 && private < 2) {
										
										/* respond accordingly to client */
										sprintf(output, "NIOJ %d \r\n", room_id);
										send(connfd, output, strlen(output), 0);
										
										/* alert users that the new client entered */
										bzero(output, MAX_INPUT);
										sprintf(output, "ECHO server %s has connected. \r\n", user_pool.user_list[i].name);
										echo(output, room_id + 2);
										
										if (echo_flag == TRUE) 
											printf("\x1B[1;34mserver> %s has connected.\x1B[0m\r\n", user_pool.user_list[i].name);
										
										/* set users state to room */
										user_pool.user_list[i].state = room_id + 2;
										
										/* increase room size */
										room_list[room_id].total_users++;
									}
									else 
										/* private room, need a pw */
										if (private > 1)
											send(connfd, "ERR 61 Invalid Password. \r\n", 27, 0);
										else
											/* not a valid room */
											send(connfd, "ERR 20 Room does not exist. \r\n", 30, 0);
								}
							}
						}
					}
					else if (!strcmp(tok, "LEAVE")) {
						
						/* leave the room */
						
						/* get room id */
						int room_id = user_pool.user_list[i].state - 2;
						
						/* see if user is in the waiting room */
						if (user_pool.user_list[i].state < 2) 
							send(connfd, "ERR 60 Not in a room. \r\n", 24, 0);
						else {	
						
							/* Send the user an evael */
							send(connfd, "EVAEL \r\n", 9, 0);
						
							/* decrement room users */
							room_list[room_id].total_users--;
							
							/* put user in waiting room */
							user_pool.user_list[i].state = 0;
							
							/* see if leaving user is room owner */
							if (room_list[room_id].owner == i) 
								replace_owner(room_id, i);								

							
							/* send echo to all users that this client left */
							sprintf(output, "ECHO server %s has disconnected. \r\n", user_pool.user_list[i].name);
							echo(output, room_id + 2);
							
							if (echo_flag == TRUE) 
								printf("\x1B[1;34mserver> %s has disconnected.\x1B[0m\r\n", user_pool.user_list[i].name);
						}
					}
					else if (!strcmp(tok, "KICK")) {
						/* kick a user */
						
						/* see if user is in a room  */
						if (user_pool.user_list[i].state < 2) 
							send(connfd, "ERR 60 Not in a room. \r\n", 28, 0);
						else {
							/* get room id */
							int room_id = user_pool.user_list[i].state - 2;
						
							/* see if user requesting kick is room owner */
							if (room_list[room_id].owner != i) 
								send(connfd, "ERR 40 Not room owner. \r\n", 25, 0);
							else {
								/* user is owner, see if provided name is valid */
							
								/* parse id */
								tok = strtok(NULL, " ");
								char buf[strlen(tok)];
								strcpy(buf, tok);
							
								boolean handled = FALSE;
							
								for (j=0; j <= user_pool.maxi; j++) {
									if (strcmp(user_pool.user_list[j].name, buf)==0){
										/* user exists, but are they in room? */
										handled = TRUE;
										if (user_pool.user_list[j].state == 
										  user_pool.user_list[i].state) {
											  
											 /* get room id */
											int room_id = user_pool.user_list[i].state - 2;
											  
											/* success, time to boot */
											user_pool.user_list[j].state = 0;
										  
											/* let them know they were kicked */
											send(user_pool.user_list[j].fd, "KBYE", 4,0);
										  
											/* let owner know kick was successful */
											bzero(message, MAX_INPUT);
											sprintf(message, "KCIK %s \r\n", user_pool.user_list[j].name);
											send(connfd, message, strlen(message),0);
										  
											/* send echo to all users that this client left */
											sprintf(output, "ECHO server %s has disconnected.", user_pool.user_list[i].name);
											
											if (echo_flag == TRUE)
												printf("\x1B[1;34mserver> %s has disconnected. \x1B[0m\r\n", user_pool.user_list[i].name);

											echo(output, user_pool.user_list[i].state);
											room_list[room_id].total_users--;
						
											/* see if leaving user is room owner */
											if (room_list[room_id].owner == j) 
												replace_owner(room_id, j);	

											break;
										}
										else {
											/* user isn't in room */
											send(connfd, "ERR 30 User not present. \r\n", 27, 0);
											break;
										}
									}
								}
								/* user isn't in room */
								if (handled==FALSE) {
									sprintf(output, "ERR 41 %s DOES NOT EXIST. \r\n", tok);
									send(connfd, output, strlen(output), 0);
								}
							}
						}
					}
					else if (!strcmp(tok, "TELL")) {
						/* private msg a user */
						
						boolean message_sent = FALSE;
						/* check to see if client is in a room */
						if (user_pool.user_list[i].state < 2) 
							send(connfd, "ERR 60 Not in a room. \r\n", 28, 0);

						else {
							
							/*get user name to send to */
							tok = strtok(NULL, " ");

							for (j=0; j <= user_pool.maxi; j++) {
								if (strcmp(user_pool.user_list[j].name, tok)==0){
									/* user exists, but are they in room? */
									
									/* set to true because either way user will get error */
									message_sent = TRUE;
									
									if (user_pool.user_list[j].state == 
									  user_pool.user_list[i].state) {
										/* success */
										bzero(message, MAX_INPUT);
										bzero(output, MAX_INPUT+12);
										
										/*get username of person who wrote msg */
										char* name = user_pool.user_list[i].name;
										
										/* edit buffer of user message */
										sprintf(message, "ECHOP %s", name);
										
										/* edit buffer of client message */
										sprintf(output, "LLET %s", tok);

										/* parse while concatenating both messages */
										while (tok != 0	) {
											tok = strtok(NULL, " ");
											if (tok != 0) {
												strcat(output, " ");
												strcat(output, tok);
												
												strcat(message, " ");
												strcat(message, tok);
											}
										}	
										
										/* send to client */
										send(connfd, output, strlen(output),0);
										
										/* connfd of user to send to */
										int user_connfd = user_pool.user_list[j].fd;
										
										/* send to user */
										send(user_connfd, message, strlen(message),0);
										break;
									}
									else {
										/* user isn't in room */
										send(connfd, "ERR 30 User not present. \r\n", 27, 0);
										break;
									}
								}
							}
							/* user isn't in room */
							if (message_sent==FALSE) {
								sprintf(output, "ERR 02 %s DOES NOT EXIST. \r\n", tok);
								send(connfd, output, strlen(output), 0);
							}
						}		
					}
					else if (!strcmp(tok, "LISTU")) {
						/* print user list */
						
						int current_room = user_pool.user_list[i].state;
						
						/* check to see if user is in waiting room */
						if (current_room < 2)
							send(connfd, "ERR 60 Not in a room. \r\n", 28, 0);
						else {
							bzero(output, MAX_INPUT + 12);
							sprintf(output, "UTSIL ");
							
							for (i=0; i <= user_pool.maxi; i++) 
								/* build list */
								if (user_pool.user_list[i].state == current_room && 
								  user_pool.user_list[i].fd >= 0) {
									strcat(output, user_pool.user_list[i].name);
									strcat(output, " \r\n");
								  }
								  
							/* finish list. at the very least, user requesting list is on it */
							strcat(output, "\r\n");
							
							/* send list */
							send(connfd, output, strlen(output), 0);
						}
					}
					else if (!strcmp(tok, "CREATEP")) {
						
						/* check to see if client is in a room */
						if (user_pool.user_list[i].state > 1)  
							send(connfd, "ERR 60 In a room. \r\n", 24, 0);
						else {
											
							bzero(output, MAX_INPUT + 12);
							char password[MAX_INPUT];
							bzero(password, MAX_INPUT);
						
							/* parse room name */
							tok = strtok(NULL, " ");
							strcpy(output, tok);
							
							/* parse password */
							tok = strtok(NULL, " ");
							strcpy(password, tok);
							
							while (tok != NULL) {
								tok = strtok(NULL, " ");
								if (tok != NULL && strcmp(tok, "\r\n")) {
									strcat(password, " ");
									strcat(password, tok);
								}
							}
					
							int room_valid;
						
							/* attempt to create a room */
							if (strlen(output) > 2)
								room_valid = create_room(output, 2, password);
							else{
								send(connfd, "ERR 60 INVALID OPERATION. \r\n\0", 29, 0);
								room_valid = -3;
							}
						
							/* VALIDATE PASSWORD HERE */
							int valid_pw = validate_password(password);
						
							/* send confirmation to client */
							if (room_valid >= 0 && valid_pw == 0) {
							
								/* add to local counter */
								room_total++;
							
								/* respond to client request */
								sprintf(message, "PETAERC %s \r\n", output);
								send(connfd, message, strlen(message), 0);
							
								/* put user in new room (each room # = index + 2)*/
								user_pool.user_list[i].state = room_valid + 2;
							
								/* set user as room owner */
								room_list[room_valid].owner = i;
							
								/* set total users to 1 */
								room_list[room_valid].total_users = 1;
							
								/* clear message buffer */
								bzero(message, MAX_INPUT);
								
								if (echo_flag == TRUE) 
									printf("\x1B[1;34mserver> %s has connected.\x1B[0m\r\n", user_pool.user_list[i].name);
						
								/* send to all users in the waiting room */
								for (j = 0; j <= user_pool.maxi; j++) {
									connfd = user_pool.clientfd[j];
									if (connfd > 0 && user_pool.user_list[j].state == 1) {
										sprintf(message, "ECHO server New Private Chat room added. \r\n");
										send(connfd, message, strlen(message), 0);
										if (echo_flag==TRUE) 
											printf("\x1B[1;34mserver> New Private Chat room added. \r\n");
									}							
								}	
							}
							else {
								if (room_valid == -1)
									send(connfd, "ERR 10 Room already exists. \r\n\0", 31, 0);
								if (room_valid == -2)
									send(connfd, "ERR 11 MAXIMUM ROOMS REACHED. \r\n\0", 33, 0);
								if (valid_pw < 0)
									send(connfd, "ERR 61 Invalid Password. \r\n", 27, 0);
							}
						}
					}
					else if (!strcmp(tok, "JOINP")) {
					
						/* join a private room */
						
						char password[MAX_INPUT];
						bzero(password, MAX_INPUT);
						
						/* see if user is in a room already */
						if (user_pool.user_list[i].state > 1)
							send(connfd, "ERR 60 Already in a room. \r\n", 28, 0);
						else {
							
							/* parse room id */
							tok = strtok(NULL, " ");
						
							/* see if they actually wrote something for id */
							if (tok == NULL) 
								send(connfd, "ERR 20 No room entered. \r\n", 30, 0);
							else {	
								int room_id = -1;
								
								char str[strlen(tok)];
								boolean valid_num = TRUE;
								strcpy(str, tok);
								
								/* see if the client entered a valid number */
								for (j=0; j < strlen(str)-1; j++) {
									if (isdigit(str[j])==0) {
										send(connfd, "ERR 20 Room does not exist. \r\n", 30, 0);
										valid_num = FALSE;
									}
								}
								
								/* parse room password */
								tok = strtok(NULL, " ");
								if (tok == NULL) 
									send(connfd, "ERR 61 Invalid password. \r\n\0", 28, 0);
								else {
									strcpy(password, tok);
									
									while (tok != NULL) {
										tok = strtok(NULL, " ");
										if (tok != NULL && strcmp(tok, "\r\n")) {
											strcat(password, " ");
											strcat(password, tok);
										}
									}
								}
								
								if (valid_num == TRUE) {
									
									/* success! number is valid. Now check if room exists */
									room_id = atoi(str);
									
									/* check password */
									boolean val = FALSE;
									if (!strcmp(password, room_list[room_id].password))
										val = TRUE;
									
							
									/* if a room name has a length of 0, it was never initialized
									 * and doesn't exist */
									if (room_id >= 0 && strlen(room_list[room_id].name)>0 && val == TRUE) {
									
										/* respond accordingly to client */
										sprintf(output, "PNIOJ %d \r\n", room_id);
										send(connfd, output, strlen(output), 0);
										
										/* alert users that the new client entered */
										bzero(output, MAX_INPUT);
										sprintf(output, "ECHO server %s has connected. \r\n", user_pool.user_list[i].name);
										echo(output, room_id + 2);
										
										if (echo_flag == TRUE) 
											printf("\x1B[1;34mserver> %s has connected. \r\n", user_pool.user_list[i].name);
										
										/* set users state to room */
										user_pool.user_list[i].state = room_id + 2;
										
										/* increase room size */
										room_list[room_id].total_users++;
									}
									else  {
										if (val==FALSE)
											send(connfd, "ERR 61 Invalid password. \r\n\0", 28, 0);
										else 
											/* not a valid room */
											send(connfd, "ERR 20 Room does not exist. \r\n", 30, 0);
									}
								}
							}
						}
					}
				}
				else {
					/* user dropped inappropriately */
					connfd = user_pool.clientfd[i];
					close(connfd);
						
					/* send echo to all users that this client left */
					sprintf(output, "ECHO server %s has disconnected. \r\n", user_pool.user_list[i].name);
					
					if (echo_flag == TRUE)
						printf("\x1B[1;34mserver> %s has disconnected. \x1B[0m\r\n", user_pool.user_list[i].name);

					if (user_pool.user_list[i].state > 1){
						echo(output, user_pool.user_list[i].state);
						room_list[(user_pool.user_list[i].state)-2].total_users--;
						
						/* get room id */
						int room_id = user_pool.user_list[i].state - 2;
						user_pool.user_list[i].state = 0;
						
						/* see if leaving user is room owner */
						if (room_list[room_id].owner == i) 
							replace_owner(room_id, i);								
					}

					/* remove username from shared list */
					remove_username(i);
					
					if (user_pool.total < 1) {
						room_exists = FALSE;
						return NULL;
					}
				}
			}
		}	
	}
		
    /* close thread (no more users in pool) */
    room_exists = FALSE;
    return NULL;
}

/** checks to see if a user name is already online **/
int check_name(char* id) {
	int i;
	
	/* cycle through list of users */
	for(i=0; i <= user_pool.maxi; i++) 
		/* if exists, return -1 */
		if (user_pool.user_list[i].name != NULL && !strcmp(user_pool.user_list[i].name, id)) 
			return 1;
	
	return 0;
}

/** create initial pool of users that is empty **/
void init_pool() {
	int i;
	
	/* set max fd to 0 */
	user_pool.maxfd = 0;
	
	/* set maxi to zero */
	user_pool.maxi = 0;
	
	/* set total to 0 */
	user_pool.total = 0;
	
	/* set all file descriptors to -1 */
	for (i=0; i < FD_SETSIZE; i++) {
		user_pool.clientfd[i] = -1;
		user_pool.user_list[i].fd = -1;
		user_pool.user_list[i].state = 0;
		bzero(user_pool.user_list[i].name, MAX_INPUT);
	}
	
	/* clear read_set */
	FD_ZERO(&user_pool.read_set);
	
	/* add listening port fd to read_set */
	FD_SET(port, &user_pool.read_set);
	
}

/** adds a client to the user pool if there is room **/
void add_client(int connfd, char* user_id) {
	int i;
	
	/* find a spot for new user */
	for (i = 0; i < FD_SETSIZE; i++) {
		if (user_pool.clientfd[i] < 0) {
			user_pool.clientfd[i] = connfd;
			
			/* add descriptor to set */
			FD_SET(connfd, &user_pool.read_set);
			
			/* add user to list */
			strcpy(user_pool.user_list[i].name, user_id);
			user_pool.user_list[i].fd = connfd;

			/* change maxfd if needed */
			if (user_pool.maxfd < connfd)
				user_pool.maxfd = connfd;
			
			/* change maxi if needed*/
			if (i > user_pool.maxi)
				user_pool.maxi = i;
			
			/* add to pool total */
			user_pool.total++;
			
			break;
		}
	}
	
	if (i == FD_SETSIZE) {
		/* send user error. max users on server */
		char error_msg[MAX_INPUT];
		bzero(error_msg, MAX_INPUT);
		sprintf(error_msg, "ERR 00 SORRY %s \r\n", user_id);
		send(connfd, error_msg, strlen(error_msg), 0);
	}
}

/** looks up username and removes from list **/
int remove_username(int id) {

	if (user_pool.clientfd[id] >= 0) {
		/* clear bit on read set for fd */
		FD_CLR(user_pool.clientfd[id], &user_pool.read_set);
						
		/* set pools fd list to -1 so next user can take spot */
		user_pool.clientfd[id]=-1;
						
		/* reset all fields */
		user_pool.user_list[id].fd = -1;
		user_pool.user_list[id].state = 0;
		bzero(user_pool.user_list[id].name, MAX_INPUT);
		user_pool.total--;
		return 1;
	}
	return -1;
}
	
/** echo messages to all users **/
int echo(char* msg, int room) {
	/* echos to all active users */
	int i;
	
	for (i=0; i <= user_pool.maxi; i++) 
		if (user_pool.clientfd[i] > 0 && user_pool.user_list[i].state==room) 
			send(user_pool.clientfd[i], msg, strlen(msg), 0);
	return 0;
}

/** initializes all rooms to -1 **/
void init_rooms() {
	int i;
	
	for (i=0; i < max_rooms; i++) {
		room_list[i].id = -1;
		bzero(room_list[i].name, MAX_INPUT);
		room_list[i].owner = -1;
		room_list[i].total_users = 0;
		room_list[i].type = 1;
		bzero(room_list[i].password, MAX_INPUT);
	}
}

/** create a new room, returns -1 on failure, and room id on success **/
int create_room(char* name, int type, char* pw) {
	int i;
	int j;
	boolean room_failure = FALSE;
						
	/* see if there are any available rooms */
	for (i=0; i < max_rooms; i++) {
		if (room_list[i].id < 0) {
								
			/* see if name is taken */
			for (j=0; j < max_rooms; j++) 
				if (!strcmp(name, room_list[j].name)) 
					room_failure = TRUE;
							
			if (room_failure == FALSE) {
				/* create the room */
				room_list[i].id = i;
				strcpy(room_list[i].name, name);
				room_list[i].type = type;
				if (pw != NULL)
					strcpy(room_list[i].password, pw);
				return i;
			}	
			else 
				return -1;
		}
	}
	/* if we are here we can't create the room */
	return -2;
}

/** replaces room owner with another user. returns 0 if ownership changed and 
 ** 1 if room destroyed **/
void replace_owner(int room_id, int i) {
	int j; boolean failed = TRUE;
	
	/* see if there are other users to take ownership */
	if (room_list[room_id].total_users >= 0) {
		for (j=0; j <= user_pool.maxi; j++) {
			/* see if next found user is in this room */
			if (user_pool.user_list[j].state == (room_id+2)){
				/* make them new owner, reduce total users 
				 *by 1 and exit loop */
				room_list[room_id].owner = j;
				failed = FALSE;
			}
		}
	}
	if (failed == TRUE) {
		/* no other users. Destroy room */
		room_list[room_id].id = -1;
		bzero(room_list[room_id].name, MAX_INPUT);
		room_list[room_id].owner = -1;
		room_list[room_id].total_users = 0;
		room_total--;
	}
}


/** looks to see if the password is correct **/
int save_user(char* id, char* pw) {
	/* I ain't neva scared */
	char a[15];
	RAND_add((unsigned char*)a, 15, 3432.234);
	
	/* create salt */
	RAND_bytes((unsigned char*)a, 15);
	
	/* HANDLE IF RAND_BYTES FAILS */
	
	/* append to password */
	size_t length = strlen(pw) + strlen(a);
	char buf[length];
	strcpy(buf, pw);
	strcat(buf, a);
	
	/* run hash function on newly created psueod-pw */
	char md[20];
	
	SHA_CTX c;
    SHA1_Init(&c);
    SHA1_Update(&c, (unsigned char*)buf, length);
    SHA1_Final((unsigned char*)md, &c);
	
	/* IF ANY SHA STUFF FAILS RETURN 1 INTERNAL ERROR */

	/* save username, key, and salt to file */
	
	//"INSERT INTO tbl1 (user, salt, key)" <== come back to for sqlite3

	/* open the file & write to it*/
	FILE* file;
    file = fopen("users.txt", "a");
	
	fprintf(file, "%s %s %%%%%%%%%%%% %s %%%%%%%%%%%% \r\n", id, a, md);
	
	fclose(file);
	
	return 0;
	
}

/** check database for whether or not name exists **/
char* find_name(char* id) {
	
	/* open the file & read it*/
	FILE* file;
    file = fopen("users.txt", "r");
	int read;
	char* super_buf;
	size_t l = 0;
	char* tok;
	char* ptr;
	
	
	while ((read = getline(&super_buf, &l, file)) != -1) {
		tok = strtok_r(super_buf, " ", &ptr);
        if(!strcmp(tok, id)) {
			fclose(file);
			return ptr;
		}
		
   }
   
	fclose(file);
	return NULL;
}

boolean user_authentication(char* e_pw, char* pw) {
	
	/* parse the line from pw into salt and key */
	char* salt = strtok(pw, " %%%%%%%%%%%% ");
	char* key = strtok(NULL, " %%%%%%%%%%%% ");
	
	int length = strlen(e_pw) + strlen(salt);
	char buf[length+1];
	bzero(buf, length+1);
	
	/* combine pw and salt */
	strcpy(buf, e_pw);
	strcat(buf, salt);
	
	char a[15];
	RAND_add(a, 15, 3432.234);
	
	/* run hash on buf */
	char md[20];
	
	SHA_CTX c;
    SHA1_Init(&c);
    SHA1_Update(&c, (unsigned char*)buf, length);
    SHA1_Final((unsigned char*)md, &c);
	
	if(!strcmp(md, key))
		return TRUE;
	
	return FALSE;
}

/** looks to see if the password is correct **/
int validate_password(char* pw) {
	int i;
	int upper=0, symbol=0, number=0;
	char buf[strlen(pw)];
	
	if (strlen(pw) < 5)
		return -1;
	
	strcpy(buf, pw);
	
	for(i=0; i < strlen(pw); i++) {
		if (isdigit(buf[i]))
			number=1;
		if (isupper(buf[i]))
			upper=1;
		if ((buf[i] >= '!' && buf[i] <= '/') || (buf[i] >= ':' && buf[i] <= '@')
		  || (buf[i] >= '[' && buf[i] <= '`') ||(buf[i] >= '{' && buf[i] <= '~'))
			symbol = 1;
	}
	
	if (upper + symbol + number == 3)
		return 0;
	
	return -1;
}