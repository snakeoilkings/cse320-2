Kathryn Blecher
108871623
kblecher

Joshua Downes
108671032
jdownes

Group name: goldteam

1 LATE DAY USED 


--------------------------------------

Included files:

server.c
client.c
client.h
Makefile
README.md
users.txt

--------------------------------------

Notes:

users.txt MUST be in same directory as files

1. There is an error causing first few login attempts to fail on a new system.
Upon the first success, all subsequent runs work flawlessly even between server runs.

2. TELL seems to do more than it should (more output).

3. Strange runtime bug during createp can cause rooms to fill up. Haven't
detected a cause and hard to replicate error.

4. Last minute connection issues when testing over a network - even with
vmware bridged connection, not seemingly working. could be just vmware but
it might not. hopefully just a vmware issue

