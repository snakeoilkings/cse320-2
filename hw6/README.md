Kathryn Blecher
108871623
kblecher

Joshua Downes
108671032
jdownes

Group name: goldteam


--------------------------------------

Included files:

server.c
client.c
threadpool.c
threadpool.h
client.h
Makefile
README.md
users.txt
auth.log

--------------------------------------

Notes:

users.txt, auth.log MUST be in same directory as files

thread pool causing a seg fault issue when tested with multiple users and has been disabled.
therefore there is no threadpool :(
