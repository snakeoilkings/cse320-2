#include "threadpool.h"

void* job_thread(void* vargp);

/**
* Creates a thread pool. More than one pool can be created.
* @param min Minimum number of threads in the pool.
* @param max Maximum number of threads in the pool.
* @param linger Number of seconds that idle threads can linger before
* exiting, when no tasks come in. The idle threads can only exit if
* they are extra threads, above the number of minimum threads.
* @param attr Attributes of all worker threads. This can be NULL.
* @return Returns a pool_t if the operation was successful. On error,
* NULL is returned with errno set to the error code.
*/
pool_t* pool_create(uint16_t min, uint16_t max, uint16_t linger, pthread_attr_t* attr) {
	pool_t* new = malloc(sizeof(pool_t));
	
	pthread_cond_t condition_busy = PTHREAD_COND_INITIALIZER;
	pthread_cond_t condition_work = PTHREAD_COND_INITIALIZER;
	pthread_cond_t condition_wait = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t thread_mutex = PTHREAD_MUTEX_INITIALIZER;

	pthread_attr_t at;
	
	new->pool_forw = new;
	new->pool_back = new; 
	new->pool_mutex = thread_mutex;
	new->pool_busycv = condition_busy; 
	new->pool_workcv = condition_work; 
	new->pool_waitcv = condition_wait;	
	new->pool_active = NULL;
	new->pool_head = NULL; 
	new->pool_tail = NULL; 
	
	if (attr != NULL)
		new->pool_attr = *(attr);
	else
		new->pool_attr = at;
	
	new->pool_flags = POOL_WAIT;
	new->pool_linger = linger; 
	new->pool_minimum = min; 
	new->pool_maximum = max; 
	new->pool_nthreads = 0; 
	new->pool_idle = 0; 

	return new;
}

/**
* Enqueue a work request to the thread pool job queue.
* If there are idle worker threads, awaken one to perform the job.
* Else if the maximum number of workers has not been reached,
* create a new worker thread to perform the job.
* Else just return after adding the job to the queue;
* an existing worker thread will perform the job when
* it finishes the job it is currently performing.
* @param pool A thread pool identifier returned from pool_create().
* @param func The task function to be called.
* @param arg The only argument passed to the task function.
* @return Returns 0 on success, otherwise -1 with errno set to the error
code.
*/
int pool_queue(pool_t* pool, void* (*func)(void *), void* arg){
	
	/* start lock (1 at a time, & can't queue when destroy has mutex and vice versa) */
	pthread_mutex_lock(&(pool->pool_mutex));
	
	job_t* n_job = malloc(sizeof(job_t));
	job_t new_job = *n_job;
	
	new_job.job_next = NULL;
	new_job.job_func = func;
	new_job.job_arg = arg;
	

	/* link job in linked list */
	if (pool->pool_head == NULL) {
		pool->pool_head = &new_job;
		pool->pool_tail = &new_job;
	}
	else {
		(pool->pool_tail)->job_next = &new_job;
		pool->pool_tail = &new_job;
	}	
	
	if (pool->pool_idle < 1 && (pool->pool_maximum - pool->pool_nthreads) > 0) {
		/* there are no idle threads but there is room to make one */

		/* create new worker thread */
		pthread_t tid;
		if (pthread_create(&tid, NULL, job_thread, pool)==0){
			(pool->pool_nthreads)++;
		}
		else {
			pthread_mutex_unlock(&(pool->pool_mutex));
			return -1;
		}  
	}
	
	else if (pool->pool_idle < 1 && (pool->pool_maximum - pool->pool_nthreads) <= 0) {
		/* no idle threads and no room for more threads, return */
	}
	
	else {
		/* there are idle workers available */
		pool->pool_idle--;
		pthread_cond_signal(&(pool->pool_workcv)); 
	}
	
	pthread_mutex_unlock(&(pool->pool_mutex));
	return 0;
}

/**
* Wait for all queued jobs to complete in the thread pool.
* @param pool A thread pool identifier returned from pool_create().
**/
void pool_wait(pool_t *pool){
	
	while (1) {
		/* lock thread */
		pthread_mutex_lock(&(pool->pool_mutex)); 

		/* see if we are done */
		if (pool->pool_nthreads == 0) {
			pthread_mutex_unlock(&(pool->pool_mutex));
			return;
		}
		pthread_mutex_unlock(&(pool->pool_mutex));
	}
	
}

/**
* Cancel all queued jobs and destroy the pool. Worker threads that are
* actively processing tasks are cancelled.
* @param pool A thread pool identifier returned from pool_create().
**/
void pool_destroy(pool_t *pool){
	/* set flag to destroy */
	pool->pool_flags = POOL_DESTROY;
	int i;
	
	/* start lock */
	pthread_mutex_lock(&(pool->pool_mutex));
	
	/* delete queue */
	job_t* t = pool->pool_head;
	job_t* next = t;
	for(i=0; i < pool->pool_nthreads; i++) {
		next = t->job_next;
		free(t);
		t = next;
	}
	
	/* kill every active thread */
	for (i=0; i < pool->pool_maximum; i++) {
		pthread_cancel((pool->pool_active)->active_tid);
		pool->pool_active = (pool->pool_active)->active_next;
		if (pool->pool_active == NULL)
			break;
	}
	
	free(pool);
	
	/* end lock */
	pthread_mutex_unlock(&(pool->pool_mutex));
	
}

void* job_thread(void* vargp) {
	/* f this crap I'm just taking the whole pool */
	pool_t* pool = (pool_t*)vargp;
	job_t* current_job;
	
	struct timespec timeout;
		
	/* run func on first nonrunning thread */
	
	while (1) {
		/* lock thread to clean up */
		pthread_mutex_lock(&(pool->pool_mutex));  
		current_job = pool->pool_head;
		pool->pool_head = current_job->job_next;
		 
		/* unlock thread */
		pthread_mutex_unlock(&(pool->pool_mutex)); 
	
		current_job->job_func(current_job->job_arg);
		
		/* now wait until someone uses this thread again, or until it times out */
		pool->pool_idle++;
		clock_gettime(CLOCK_REALTIME, &timeout);
		timeout.tv_nsec += (pool->pool_linger) * 1000000;
		
		//pthread_cond_timedwait(&(pool->pool_workcv), &(pool->pool_mutex), &timeout);
		pthread_cond_wait(&(pool->pool_workcv), &(pool->pool_mutex));
		pthread_mutex_unlock(&(pool->pool_mutex));
		
		/* check for time out, and delete thread if so */
		
	}
}

