#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <wait.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <netdb.h>
#include <pthread.h>
#include <arpa/inet.h>

#define TRUE 1
#define FALSE 0

/* Assume no input line will be longer than 1000 bytes */
#define MAX_INPUT 1001

typedef char boolean;
typedef struct sockaddr SA;

int open_clientfd(char *ip, char *port);
int login(char *user_name, int clientfd);
void begin_echo(int fd);
void toUpperString(char *str);
