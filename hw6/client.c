#include "client.h"
#include <ctype.h>


boolean new_login = FALSE;

int main(int argc, char **argv){
    /* File descriptor socket */
    int clientfd;
    char *user_name; // Username
    char *SERVER_IP; //Server to connect to
    char *SERVER_PORT; // Port to connect to

    /* Strings to hold message we are sending and receiving */
    char send_message[MAX_INPUT];
    char recv_message[MAX_INPUT];


    /* looks for optional flags and responds accordingly */
    extern int optind;
    int opt;

    while((opt = getopt(argc, argv, "hc")) != -1) {
        switch(opt) {
            case 'h':
                /* print out help menu and exit */
                printf("usage: ./client [-h] NAME SERVER_IP SERVER_PORT\n");
                return EXIT_SUCCESS;
			case 'c':
				/* creating a new user */
				new_login = TRUE;
            default:
                break;
        }
    }

    /* Get positional arguments */
    if(optind < argc && (argc - optind) == 3) {
        /* sets port = to first argument */
        user_name = argv[optind++];
        /* sets SERVER_IP to second argument */
        SERVER_IP = argv[optind++];
        /* sets SERVER_PORT to third */
        SERVER_PORT = argv[optind++];
    } else {
        fprintf(stderr, "Invalid arguments. Usage: ./client [-h] USERNAME IP_ADDRESS PORT_NUMBER \n");
        exit(EXIT_FAILURE);
    }

    /* Attempt to create socket file descriptor */
    clientfd = open_clientfd(SERVER_IP, SERVER_PORT);

    /* Exit if invalid clientfd */
    if(clientfd == -1){
        printf("Something went wrong...\n");
        return EXIT_FAILURE;
    }

    /* Attempt to log in */
    if((login(user_name, clientfd) == 0)){
        /* We successfully logged in */
        printf("Successfully logged in as %s\n", user_name);
    }
    else{
        printf("Failure to log in. Exitting...\n");
        // return EXIT_FAILURE;
    }

    /* Start echoing */
    begin_echo(clientfd);

    while(TRUE)
    {
        /* Clear send and receive */
        bzero(&send_message, MAX_INPUT);
        bzero(&recv_message, MAX_INPUT);

        /* Read into send_message until newline */
        fgets(send_message, MAX_INPUT, stdin);

        /* Write message to socket fd, read and print response */
        write(clientfd, send_message, strlen(send_message) + 1);
        read(clientfd, recv_message, 100);
        printf("%s", recv_message);
    }
}

int open_clientfd(char* ip, char* port){
    int clientfd;
    struct addrinfo hints, *listp, *p;

    /* Get a list of potential server addresses */
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_socktype = SOCK_STREAM; /* Open a connection */
    hints.ai_flags = AI_NUMERICSERV; /*... using numeric port arg */
    hints.ai_flags |= AI_ADDRCONFIG; /* Recommended for connections */
    getaddrinfo(ip, port, &hints, &listp); 

    /* Walk list for socketfd we can connect to */
    for(p = listp; p; p = p->ai_next){
        /* Create socket descriptor */
        if((clientfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0)
            continue; /* Socket failed, try the next */

        /* Connect to server */
        if(connect(clientfd, p->ai_addr, p-> ai_addrlen) != -1)
            break; /* Success */
        close(clientfd); /* Failure, try the next one */
    }

    /* Clean up */
    freeaddrinfo(listp);
    if(!p) /* All connects failed */
        return -1;
    else
        return clientfd;
}

int login(char *user_name, int clientfd){
    /* Lines for messages */
    char recv_line[MAX_INPUT];
	char send_pass[MAX_INPUT];
    char send_name[MAX_INPUT];
	bzero(send_name, MAX_INPUT);
	bzero(send_pass, MAX_INPUT);
	
    /* Send and receive initial verbs */
    write(clientfd, "ALOHA! \r\n", 10);
    bzero(&recv_line, MAX_INPUT);
    int r = read(clientfd, recv_line, MAX_INPUT);
	if (r <= 0) {
		close(clientfd);
		printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
		exit(EXIT_FAILURE);
	}
	
    if((strcmp(recv_line, "!AHOLA \r\n")) == 0){
		
		/** added code for login authentication **/
	
		if (new_login == FALSE) {
			/* Attempt to login with username */
			sprintf(send_name, "IAM %s \r\n", user_name);
			write(clientfd, send_name, strlen(send_name));
		
			/* Check for "AUTH" */
			bzero(&recv_line, MAX_INPUT);
			r = read(clientfd, recv_line, MAX_INPUT);
			if (r <= 0) {
				close(clientfd);
				printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
				exit(EXIT_FAILURE);
			}

            char *recv_verb = strtok(recv_line, " ");
			if((strncmp(recv_verb, "AUTH", 4) == 0)){
				write(STDOUT_FILENO, "Enter password: ", 16);
				bzero(recv_line, MAX_INPUT);
				r = read(STDIN_FILENO, recv_line, MAX_INPUT);
				if (r <= 0) {
					close(clientfd);
					printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
					exit(EXIT_FAILURE);
				}
				/* remove endline character */
				recv_line[strlen(recv_line)-1] = '\0';
				
				/* send password to server */
				sprintf(send_pass, "PASS %s \r\n", recv_line);
				write(clientfd, send_pass, strlen(send_pass));
			}
			else {
				printf("\x1B[1;31mERR 02 %s DOES NOT EXIST.\x1B[0m \n", user_name);
				exit(EXIT_FAILURE);
			}
		}
		else {
			/* Attempt to login with username */
			sprintf(send_name, "IAMNEW %s \r\n", user_name);
			write(clientfd, send_name, strlen(send_name));
		
			/* Check for "HINEW" */
			bzero(&recv_line, MAX_INPUT);
			r = read(clientfd, recv_line, MAX_INPUT);
			if (r <= 0) {
				close(clientfd);
				printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
				exit(EXIT_FAILURE);
			}
			if((strcmp(strtok(recv_line, " "),"HINEW") == 0)){
				write(STDOUT_FILENO, "Enter password: ", 16);
				bzero(recv_line, MAX_INPUT);
				r = read(STDIN_FILENO, recv_line, MAX_INPUT);
				if (r <= 0) {
					close(clientfd);
					printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
					exit(EXIT_FAILURE);
				}
				
				/* remove endline character */
				recv_line[strlen(recv_line)-1] = '\0';
			
				/* send password to server */
				sprintf(send_pass, "NEWPASS %s \r\n", recv_line);
				write(clientfd, send_pass, strlen(send_pass));
			}
			else {
				printf("\x1B[1;31mERR 01 SORRY %s.\x1B[0m \n", user_name);
				exit(EXIT_FAILURE);
			}
		}
		
        /* Check for "HI" */
        bzero(&recv_line, MAX_INPUT);
        r = read(clientfd, recv_line, MAX_INPUT);
		if (r <= 0) {
			close(clientfd);
			printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
			exit(EXIT_FAILURE);
		}
        /* Successful login if we have a "HI" */
        if((strcmp(strtok(recv_line, " "),"HI") == 0))
            return 0;
        else{
            char *err = strtok(NULL, "");
            printf("\x1B[1;31m%s", err);
            printf("\x1b[0m");
            fflush(stdout);
            exit(EXIT_FAILURE);
        }
    }

    return -1;
}

void begin_echo(int fd){

    /* buffer to hold stuff */
    char buf[MAX_INPUT];
    char msg[MAX_INPUT+4];

    while(TRUE){

        /* Zero out the buffer */
        bzero(buf, sizeof(buf));
        bzero(msg, sizeof(msg));

        /* File descriptor set for reading from (i.e. stdin and server) */
        fd_set readfds;
        /* Zero fd set */
        FD_ZERO(&readfds);
        /* Add stdin and fd to set */
        FD_SET(fd, &readfds);
        FD_SET(STDIN_FILENO, &readfds);

        /* Get max */
        int max = STDIN_FILENO;
        if(fd > STDIN_FILENO){
            max = fd;
        }
        /* Wait for an action from a file descriptor (might need timeout here) */
        select(max + 1, &readfds, NULL, NULL, NULL);
        if(FD_ISSET(STDIN_FILENO, &readfds)){
            read(STDIN_FILENO, buf, MAX_INPUT);
            /* Send message to server with respective verb */

            strcpy(msg, buf);
            char *verb = strtok(msg, " ");

            /** ADDED FOR TESTING PURPOSES **/
           
            if (!strncmp(verb, "CREATER", 7) || !strncmp(verb, "LISTR", 5) || !strncmp(verb, "LISTU", 5)
                    || !strncmp(verb, "JOIN", 4) || !strncmp(verb, "LEAVE", 5)) {
                sprintf(msg, "%s \r\n", buf);
                write(fd, msg, MAX_INPUT);
            }
			
			/** CAN REMOVE ABOVE **/
			
            else if(!strncmp(verb, "/creater", 8)){
                char *room_name = strtok(NULL, "");
                sprintf(msg, "CREATER %s \r\n", room_name); 
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/leave", 6)){
                sprintf(msg, "LEAVE \r\n");
                write(fd, msg, MAX_INPUT);
            }
			else if(!strncmp(verb, "/joinp", 6)){
				char *room_id = strtok(NULL, " ");
                char *room_auth = strtok(NULL, "\n");
                sprintf(msg, "JOINP %s %s \r\n", room_id, room_auth);
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/join", 5)){
                char *room_id = strtok(NULL, "");
                sprintf(msg, "JOIN %s", room_id);
                /* get rid of newline */
                fflush(stdout);
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/listrooms", 10)){
                sprintf(msg, "LISTR \r\n");
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/quit", 5)) {
                printf("Logging out...\n");
                write(fd, "BYE \r\n", 8);
				exit(0);
            }
            else if(!strncmp(verb, "/kick", 5)){
                char *member = strtok(NULL, "\n");
                sprintf(msg, "KICK %s \r\n", member);
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/tell", 5)){
                char *member = strtok(NULL, " ");
                char *message = strtok(NULL, "\n");
                sprintf(msg, "TELL %s %s \r\n", member, message);
                write(fd, msg, MAX_INPUT);
            }
            else if(!strncmp(verb, "/listusers", 10)){
                write(fd, "LISTU \r\n", MAX_INPUT);
            }
			else if(!strncmp(verb, "/createp", 8)){
				char *room_name = strtok(NULL, " ");
                char *room_password = strtok(NULL, "\n");
                sprintf(msg, "CREATEP %s %s \r\n", room_name, room_password);
                write(fd, msg, MAX_INPUT);
            }

            else {
                sprintf(msg, "MSG %s \r\n", buf);
                write(fd, msg, MAX_INPUT);
            }

        }
        if(FD_ISSET(fd, &readfds)){
            bzero(buf, MAX_INPUT);
            int r = read(fd, buf, MAX_INPUT);
			if (r <= 0) {
				close(fd);
				printf("\x1B[1;31mERR: Connection to server lost.\x1B[0m\n");
				exit(EXIT_FAILURE);
			}
            /* Parse and respond based on verb */
            strcpy(msg, buf);
            char* sent[1];
            char *verb = strtok_r(msg, " ", sent);
            if(!strncmp(verb, "ERR", 3)){
                    printf("\x1B[1;31m%s", buf);
                    printf("\x1b[0m");
            }
            else if((strncmp(verb, "ECHO", 4)) == 0){
                char* sent_by_orig = strtok_r(NULL, " ", sent);
                char* echo_message = strtok_r(NULL, "", sent);
                /* This is only here to make sure server messages are printed as such regardless of case */
                char sent_by[strlen(sent_by_orig)];
                strcpy(sent_by, sent_by_orig);
                toUpperString(sent_by);
                if(!strncmp(verb, "ECHOP", 5)){
                    printf("\x1B[1;35m%s> %s", sent_by_orig, echo_message);
                    printf("\x1B[0m");
                }
                if(!strncmp(sent_by, "SERVER", 6)){
                    printf("\x1B[1;34m%s> %s", sent_by_orig, echo_message);
                    printf("\x1b[0m");
                }
                else{
                    printf("%s> %s", sent_by_orig, echo_message);
                }   
            }
            else if((strncmp(verb, "LLET", 4))==0){
                char *sent_by = strtok_r(NULL, " ", sent);
                char *private_message = strtok_r(NULL, "", sent);
                printf("\x1B[1;35m%s> %s", sent_by, private_message);
                printf("\x1b[0m");
            }
            else if((strncmp(verb, "BYE", 3))==0){
                exit(0);
            }
            else if((strncmp(verb, "RETAERC", 7) == 0)){
                char* room = strtok_r(NULL,"",sent);
                printf("Created %s",room);
            }
            else if(strncmp(verb, "NIOJ", 4) == 0){
                char *id = strtok_r(NULL, "", sent);
                printf("Joined room ID:%s", id);
            }
            else if((strncmp(verb, "RTSIL", 5) == 0)){
                    char* room_name;
                    char* room_id;
                    char* room_type;
                    char* carriage_ret;
                    printf("Room\tID\tType\r\n");
                    while((room_name = strtok_r(NULL, " ", sent)) && (room_id = strtok_r(NULL, " ", sent)) && (room_type = strtok_r(NULL, " ", sent)) && (carriage_ret= strtok_r(NULL, " ", sent)) ){
                            //Get rid of newline character
                            int len = strlen(room_name);
                            room_name[len - 1] = '\0';
							if (*(room_type) == '2')
								printf("\x1B[1;35m%s\t%s\t%s%s\x1B[0m", room_name, room_id, room_type, carriage_ret);
							else
								printf("%s\t%s\t%s%s", room_name, room_id, room_type, carriage_ret);
                    }
                if(room_name != NULL && (strncmp(room_name, "no_rooms", 8)) == 0){
                    printf("No rooms exist. Create one with \"/createroom ROOMNAME\"\n");
                }
            }
            else if((strncmp(verb, "UTSIL", 5) == 0)){
                char* users = strtok_r(NULL, "", sent);
                printf("Users:\n%s",users);
            }
            else if((strncmp(verb, "EVAEL", 5) == 0)){
                printf("Room left.\n");
            }
            else if((strncmp(verb, "KCIK", 4)==0)){
                printf("\x1B[1;34mserver> User was successfully kicked.\n");
                printf("\x1b[0m");
                fflush(stdout);
            }
            else if((strncmp(verb, "KBYE", 4)==0)){
                printf("\x1B[1;34mServer> Attention: You have been kicked from the room.\n");
                printf("\x1b[0m");
                fflush(stdout);
            }
			else if((strncmp(verb, "PETAERC", 7)==0)){
                char* room = strtok_r(NULL,"",sent);
                printf("Created %s",room);
                fflush(stdout);
            }
			else if((strncmp(verb, "PNIOJ", 5)==0)){
                char *id = strtok_r(NULL, "", sent);
                printf("Joined room ID:%s", id);
                fflush(stdout);
            }
            else{
                printf("VERB: xxx%sxxx\n", verb);
                printf("Command not yet supported: %s", buf);
            }

            fflush(stdout);

            /*
            printf("%s", buf);
            printf("verb: %s\n", verb);
            printf("sent: %s\n", sent[0]);
            if((strncmp(verb, "ECHO", 4)) == 0)
                printf("\x1B[1;31mGOT THIS: %s", sent[0]);
                printf("\x1B[0m\n");
                */
        }
    }
}

void toUpperString(char* str){
    int i = 0;
    while (str[i])
    {
        str[i] = toupper(str[i]);
        i++;
    }
    return;
}
