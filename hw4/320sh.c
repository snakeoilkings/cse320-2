#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <wait.h>
#include <termios.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

/* Assume no input line will be longer than 1024 bytes */
#define MAX_INPUT 1024
/* Assume no input line will have more than 128 arguments */
#define MAX_ARGS 512
/* Only keep last 50 commands */
#define MAX_HISTORY 50


typedef char boolean; 

typedef struct processes {
    FILE *fp;
    int fdf;
    int fdp;
    int output;
    int argc;
    char **args;
    boolean runf;
    boolean runp;
    boolean piped_to;
    boolean error;
    boolean errorp;
} process;

typedef struct job {  
	int id;                    /* job ID given by shell */
	int pgid;				   /* pgid from getpgid() */
    char state;                /* current state of job (RUN, STOP, END, TERM) */
    char command[MAX_INPUT];   /* command (job running name) */
} job;

/* global vars */
volatile time_t start;
FILE* w_file;
char history_filename[1024];
int JOB_TOTAL = 0; 				/* Total current number of jobs running. */
int history_total=0;			/* total history within our history buffer */
int total_processes;			/* total processes */
int history_counter = 0;
process process_list[128];		/* total processes running. 128 is our arbitrary max */
job job_list[128];				/* total jobs running. 128 again is our arbitraty max */
char *history[MAX_HISTORY];		/* current history list */
char filename[1024];
char* q = "?";
int return_value;				/* environmental var */
boolean within_pipe=FALSE;
boolean within_redirect=FALSE;
boolean skip_last_piping = FALSE;

/* run-time flags */
boolean debug = FALSE;
boolean redirection = FALSE;
boolean piping = FALSE;
boolean job_stopped = FALSE;

/* function preprocessing list */
int builtin_command(char **argv);
boolean parse_args(char* buf, char** args);
int exec_command(boolean bg, char **args);
void sig_handler(int sig);
void create_job(pid_t id, char* command);
void delete_job(int id);
void init_jobs();
void sig_child(int sig);
void Kill(pid_t pid, int sig);
int config_id(char* buf);
void job_status(pid_t pid, char state);
pid_t get_pid(int id);
pid_t get_id(int id);
void get_filename(char** args);

/* job handlers */
pid_t shell_id;
sigset_t mask, prev_mask;
pid_t pid_chld;
pid_t fg;


int main (int argc, char ** argv, char **envp) {
	

	/* This code allows the kernel to send certain signals. By setting STDINs 
	 * termios' c_lfag = ISIG, we have gained the ability to CTRL+C/Z */
	struct termios tty_settings;
	tcgetattr(STDIN_FILENO, &tty_settings);
	tty_settings.c_lflag |=  ISIG;
	tcsetattr(STDIN_FILENO, TCSANOW, &tty_settings);
	
	/* the shell should ignore interrupts and stops */
	signal (SIGINT, sig_handler);
	signal (SIGTSTP, sig_handler);
	/* let's sig_chld hangle a SIGCHLD call */
	signal (SIGCHLD, sig_child);
	
	/* shell information */
	shell_id = getpid();
	fg = shell_id;

	/* block SIGCHLD until we want to receive it */
	sigemptyset(&mask);
	sigaddset(&mask, SIGCHLD);
	sigprocmask(SIG_BLOCK, &mask, &prev_mask);
	
	/* set up job list */
	init_jobs();
	
	int i;
	char* address;
    char cmd[MAX_INPUT];
    char buf[MAX_INPUT];
    char *args[MAX_ARGS];
	
	/* add return support */
	setenv(q, "0", 1);
	
	/* idgaf */
	void* super_buffer = calloc(51249, 1);

	/* set up list */
	history[0] = (char*)super_buffer;
	for(i=1; i < 50; i++) {
		address = super_buffer + (i * 1029);
		history[i] = address;
	}
	
    int finished = 0;
	boolean bg = FALSE;
    char *prompt = "320sh> ";
    int stdt = dup(1);
    int stder = dup(2);
//    int stdn = dup(0);

    extern int optind;
    int opt;
    while((opt = getopt(argc, argv, "d")) != -1) {
        switch(opt) {
            case 'd':
                debug=TRUE;
                break;
            default:
                break;
        }
    }

    if(optind < argc && (argc - optind) == 1) {
        FILE* file;
        file = fopen(argv[1], "r");
        char* instruction;
        size_t r=0;
        size_t length = MAX_INPUT;

        if (file != NULL) {
            while((r = getline(&instruction, &length, file))!=-1) {
                if(*instruction!='#') {
                    /* parse the argument, then execute it */
                    bg = parse_args(instruction, args);
                    exec_command(bg, args);
                }
                else
                    /* it's a comment, ignore */
                    continue;
            }
        }
    }
	
	/* opens a save file for history */
	
	/* gets home path */
    char* home = getenv("HOME");
	strcpy(history_filename, home);	
	strcat(history_filename, "/320sh_history.txt");
	/* tries to open file path/320sh_history.txt */
	/* view cursor for history */
	w_file = fopen(history_filename, "r+");
	/* if the file doesn't exist, create it */
	if (w_file==NULL)
		w_file = fopen(history_filename, "wd");
	else {
		char* buff;
		size_t length;
		int read;
		/* if it does exist, pull it's contents into history buffer */
		for(i=0; i < 50; i++) {
			read = getline(&buff, &length, w_file);
			if (read < 0)
				break;
			address = history[i];
			strcpy(address, buff);
			history_total++;
			history_counter++;
       }
	}
	

    while (!finished) {
		int view_cursor = history_counter;
		
        char *cursor;
        char last_char;
        int rv;
        int count;


        /* Print the prompt */
        rv = write(1, prompt, strlen(prompt));
        if (!rv) { 
            finished = 1;
            break;
        }

        /* read and parse the input */
        for(rv = 1, count = 0, 
                cursor = cmd, last_char = 1;
                rv 
                && (++count < (MAX_INPUT-1))
                && (last_char != '\n');
                cursor++) { 
				
				
			/* reset flag every char */
			boolean arrow_flag=FALSE;
			
            *cursor = getchar();
			
			/* if the value of cursor is an escape sequence, it will be = 27 COME BACK TO*/ 
			if (*cursor==27) {
				/* next val will be 91 for arrow */
				int esc_1 = getchar();
				/* this determines the directionality of the arrow (65, 66, 67, 68)*/
				int esc_2 = getchar();
				if (esc_1==91 && esc_2==65) {
					
					view_cursor--;
					
					/* only move back if there is history to look at */
					if ((history_total-view_cursor)>0 && view_cursor>=0) {
						
						write(1, history[view_cursor % 49], strlen(history[view_cursor % 49]));
						
						/* save to the command */
						strcpy(cmd,history[view_cursor % 49]);
						cursor = cmd + strlen(history[view_cursor % 49])-1;
						
						
						/* return to list end */
						if (view_cursor < 0 && history_total > 49)
							view_cursor = 49;
					}
					
					if (view_cursor < 0)
						view_cursor=0;
					
					cursor--;
					
					/* let prompt know we didn't get meaningful output */
					arrow_flag=TRUE;
				}
				if (esc_1==91 && esc_2==66) {
					
					/* only move forward if there is history to look at */
					if ((history_total-view_cursor)>0) {
						
						view_cursor++;
						
						/* return to list front */
						if (view_cursor > 49)
							view_cursor = 0;
						
						if (view_cursor != (history_counter % 49)) {
							
							write(1, history[view_cursor % 49], strlen(history[view_cursor % 49]));
						
							/* save to the command */
							strcpy(cmd,history[view_cursor % 49]);
							cursor = cmd + strlen(history[view_cursor % 49])-1;
							
						}
					}
					cursor--;
					
					/* let prompt know we didn't get meaningful output */
					arrow_flag=TRUE;
				}
			}	
			
            last_char = *cursor;
			
			/* Write cursor to terminal */
			if (arrow_flag==FALSE)
				write(1, cursor, 1);
			else {
				count--;
				last_char = '\0';
			}
        } 
		
			
        *cursor = '\0';

        if (!rv) { 
            finished = 1;
            break;
        }
		/*save command in history list */
		strcpy(history[history_counter % 50], cmd);
		history_counter++;
		history_total++;
		
		/* save history in file */   
		fileno(w_file);
		fseek(w_file, 0, SEEK_SET);
		/* add each line */
		for(i = history_counter-1; i > -1; i--) 
		    fprintf(w_file, "%s", history[i % 50]);

		/* copy the user input into a buffer */
        strcpy(buf, cmd);
        
		/* check if it's a bg process */
        boolean bg = parse_args(buf, args);
		
		if(piping == TRUE){
					
			int stdt = dup(1);
			int stdn = dup(0);
					
			/* Creates file descriptors for each pipe*/
			int pipefd[total_processes][2];
			int i;
			
			if (debug == TRUE)
                    fprintf(stderr,"RUNNING: %s\n", args[0]);
			
			within_pipe=TRUE;
			/* start a new process */
			pid_t pid = fork();
			
			if (pid==0) {
				
				/* creates new process group for the job */
				setpgid(0,0);
				
				for(i = 0; i < total_processes; i++){
                                    
                                    /* Check if we are on our second to last process and redirection*/
                                    if((i == total_processes - 1) && process_list[i].runf == TRUE){
                                        /* Redirection */
                                        if(process_list[i].piped_to == TRUE){
						/* Change stdin to the buffer */
						dup2(pipefd[i-1][0], 0);
						/* Set piped_to back to false */
						process_list[i].piped_to = FALSE;
					}
                                        if(process_list[i].error == TRUE){
                                                dup2(process_list[i].fdf, STDERR_FILENO);
                                                process_list[i].error = FALSE;
                                        }
                                        else{
                                                dup2(process_list[i].fdf, 1);
                                        }
                                        close(process_list[i].fdf);
                                        exec_command(0, process_list[i].args);
                                        dup2(stdt, 1);
                                        dup2(stder, 2);
                                        skip_last_piping = TRUE;
                                    }
                                    
                                    else{
					/* Generate unidirectional pipe */
					pipe(pipefd[i]);
					/* Check if pipe has been piped to*/
					if(process_list[i].piped_to == TRUE){
						/* Change stdin to the buffer */
						dup2(pipefd[i-1][0], 0);
						/* Set piped_to back to false */
						process_list[i].piped_to = FALSE;
					}
					/* Set stdout to write to pipe */
					dup2(pipefd[i][1], 1);
					close(pipefd[i][1]);
					/* Execute */
					exec_command(0, process_list[i].args);
                                    }
                                    /* Change stdin and stdout back to original*/
                                    dup2(stdn, 0);
                                    dup2(stdt, 1);
                                    /* Flush stdout*/
                                    fflush(stdout);
                                    /* Reset process */
                                    process_list[i].argc = 0;
                                    process_list[i].args = NULL;
                                    process_list[i].fdf = 0;
                                    process_list[i].fdp = 0;
                                    process_list[i].fp = NULL;
                                    process_list[i].output = 0;
                                    process_list[i].runf = FALSE;
                                    process_list[i].runp = FALSE;

                                }
                                    /* Final pipe */
                                if(skip_last_piping == FALSE){
                                    /* Set stdin to final pipe stdin */
                                    dup2(pipefd[total_processes - 1][0], 0);


                                    /* Execute final command */
                                    exec_command(0, process_list[total_processes].args);

                                    /* Change stdin back to original*/
                                    dup2(stdn, 0);
                                }
				/* Close pipes */
				close(pipefd[total_processes - 1][1]);
				close(pipefd[total_processes - 1][0]);
				
				/* end process group */
				exit(0);
			}
			
			/* handle parent */
			
			/* accept SIG_CHLD calls again */
			sigprocmask(SIG_SETMASK, &prev_mask, NULL);
			
			if (bg==FALSE)
				/* put process into foreground */
				fg = 0 - pid;

			/* creates job */                    
			create_job(pid, args[0]);
		
			if (bg==TRUE) {
				int jid = get_id(pid);
				printf("[%d] (%d) %s\n", job_list[jid].id, job_list[jid].pgid, job_list[jid].command);
			}
			
			pid_chld = 0;
				
			/* when child dies, it signals parent and changes
			 * pid_chld to 0 so the shell can continue and
			 * bg processes won't be zombies */
			 if (bg==FALSE)
				while(!pid_chld) 
					sigsuspend(&prev_mask);	
				
			sleep(1); // race condition
				
			/* output info if job stopped */
			if(job_stopped==TRUE) {
				int jid;
				job_status(pid_chld, 'S');
				jid = get_id(pid_chld);
				printf("[%d]+ Stopped              %s\n", jid+1, job_list[jid].command);
				job_stopped=FALSE;
			}
			
			char env[8]; 
			snprintf(env, sizeof(env), "%d", return_value);
			/* save return value variable */
			setenv(q, env, 1);
				
			/* return shell to foreground */
			fg = shell_id;
				
			/* set to 0 in case of unexpected termination */
			pid_chld = 0;
			
			/* Switch off piping */
			piping = FALSE;
			within_pipe=FALSE;
			/* Reset total_processes*/
			total_processes = 0;
		}
		
		/* Check for redirection */
        else if(redirection == TRUE){
            int i;
			
			within_redirect=TRUE;
			/* start a new process */
			pid_t pid = fork();
			
			if (pid==0) {
				
				/* creates new process group for the job */
				setpgid(0,0);
				
				if (debug == TRUE)
                    fprintf(stderr,"RUNNING: %s\n", args[0]);
			
				for(i = 0; i < total_processes + 1; i++){
					if(process_list[i].runf == TRUE){
						if(process_list[i].error == TRUE){
							dup2(process_list[i].fdf, STDERR_FILENO);
							process_list[i].error = FALSE;
						}
						else{
							dup2(process_list[i].fdf, 1);
						}
						close(process_list[i].fdf);
						exec_command(0, process_list[i].args);
						dup2(stdt, 1);
						dup2(stder, 2);
					}
					if(process_list[i].runp == TRUE){
						if(process_list[i].errorp == TRUE){
							dup2(process_list[i].fdp, STDERR_FILENO);
							process_list[i].errorp = FALSE;
						}
						else{
							dup2(process_list[i].fdp, 1);
						}
						close(process_list[i].fdp);
						exec_command(0, process_list[i].args);
						dup2(stdt, 1);
						dup2(stder, 2);
					}
					/* Reset process */
                
                
					process_list[i].argc = 0;
					process_list[i].args = NULL;
					process_list[i].fdf = 0;
					process_list[i].fdp = 0;
					process_list[i].fp = NULL;
					process_list[i].output = 0;
					process_list[i].runf = FALSE;
					process_list[i].runp = FALSE;
				
					exit(0);
				}
            
			
			}
			
			/*handle parent*/
			
			/* accept SIG_CHLD calls again */
			sigprocmask(SIG_SETMASK, &prev_mask, NULL);
			
			if (bg==FALSE)
				/* put process into foreground */
				fg = 0 - pid;

			/* creates job */                    
			create_job(pid, args[0]);
		
			if (bg==TRUE) {
				int jid = get_id(pid);
				printf("[%d] (%d) %s\n", job_list[jid].id, job_list[jid].pgid, job_list[jid].command);
			}
			
			pid_chld = 0;
				
			/* when child dies, it signals parent and changes
			 * pid_chld to 0 so the shell can continue and
			 * bg processes won't be zombies */
			 if (bg==FALSE)
				while(!pid_chld)
					sigsuspend(&prev_mask);	
				
			sleep(1); // race condition
				
			/* output info if job stopped */
			if(job_stopped==TRUE) {
				int jid;
				job_status(pid_chld, 'S');
				jid = get_id(pid_chld);
				printf("[%d]+ Stopped              %s\n", jid+1, job_list[jid].command);
				job_stopped=FALSE;
			}
			
			char env[8]; 
			snprintf(env, sizeof(env), "%d", return_value);
			/* save return value variable */
			setenv(q, env, 1);
				
			/* return shell to foreground */
			fg = shell_id;
				
			/* set to 0 in case of unexpected termination */
			pid_chld = 0;
		
			within_redirect=FALSE;
			redirection = FALSE;
			total_processes = 0;
				
		}
        
        else{
            exec_command(bg, args);
            fflush(stdout);
        }

    }		
    return EXIT_SUCCESS;
}

/** evaluated the command and properly executes it whether built-in or not */
int exec_command(boolean bg, char** args) {

    pid_t pid;
	
	struct stat buffer;
    int exists=1;
	
	/* checks if user just typed in '.' or './' */
	if (!strcmp(args[0], "./") || (!(strcmp(args[0], ".")) && args[1]==NULL)) {
		printf(".: usage: . filename [arguments]\n");
		return 1;
	}
	
	/* checks if the user is using way too many ///// */
	if (*args[0]=='.' && *(args[0]+1)=='/' && *(args[0]+2)=='/'){
		printf(".: usage: . filename [arguments]\n");
		return 1;
	}
		
	
	/* checks if users just typed in a <, >, or | */
	if ((!(strcmp(args[0], ">")) || !(strcmp(args[0], "<"))
		 || !(strcmp(args[0], "|"))) && args[1]==NULL) {
		printf("syntax error near unexpected token\n");
		return 1;
	}
	
	/* checks if user is just making crazy statements */
	int j;
	for (j=0; j < MAX_ARGS; j++) {
		if (args[j+1]==NULL)
			break;
		if ((!strcmp(args[j], "<") || !strcmp(args[j], ">") || !strcmp(args[j], "|"))
			&& (!strcmp(args[j+1], "<") || !strcmp(args[j+1], ">") || !strcmp(args[j+1], "|"))) {
				printf("syntax error near unexpected token\n");
				return 1;
			}
	}
    

    /* call builtin_command to check and execute built in commands */
    int built_val = builtin_command(args);
	

    /* if not a built in, execute it through a fork */
    if (strlen(args[0])!=0 && built_val==0) {

        get_filename(args);
		
		/* does the file exist? */
        exists = stat(filename, &buffer);

        /* if so, starts a new process running the program */
        if (exists==0) {
			
			/* start a new process */
			pid = fork();
			
			if (pid==0) {
				
				/* creates new process group for the job */
				if (within_redirect != TRUE || within_pipe != TRUE)
					setpgid(0,0);
				
				FILE* file;
                file = fopen(filename, "r");
				
                if (debug == TRUE)
                    fprintf(stderr,"RUNNING: %s\n", args[0]);
				
				char* instruction;
				ssize_t r=0;
				size_t length = MAX_INPUT;
				boolean shell = FALSE;
				if (file != NULL) {
					r = getline(&instruction, &length, file);
					if (r!=-1 && strcmp(instruction, "#!320sh\n")==0) 
						/* we have a shell script! */
						shell = TRUE;
				}
				if (shell == TRUE) {
					within_pipe=FALSE;
					boolean bg_shell;
					while((r = getline(&instruction, &length, file))!=-1) {
						if(*instruction!='#') {
							/* parse the argument, then execute it */
							bg_shell = FALSE;
							bg_shell = parse_args(instruction, args);
							exec_command(bg_shell, args);
						}
						else
							/* it's a comment, ignore */
							continue;
					}
					within_pipe=TRUE;
				}
				else {
					return_value = execv(filename, &args[0]);
					
					/* if we are here there was an error */
					fprintf(stderr, "execv error\n");
					exit(0);
				}
				/* this is the exit for the initial shell script */
				exit(0);
			}
		if (within_redirect==TRUE || within_pipe==TRUE)
			waitpid(-1, NULL, WUNTRACED);
		else {
		/* accept SIG_CHLD calls again */
		sigprocmask(SIG_SETMASK, &prev_mask, NULL);
			
		if (bg==FALSE)
			/* put process into foreground */
			fg = 0 - pid;

		/* creates job */                    
		create_job(pid, args[0]);
			
		if (bg==TRUE) {
			int jid = get_id(pid);
			printf("[%d] (%d) %s\n", job_list[jid].id, job_list[jid].pgid, job_list[jid].command);
		}
			
		pid_chld = 0;
				
		/* when child dies, it signals parent and changes
		 * pid_chld to 0 so the shell can continue and
		 * bg processes won't be zombies */
			 
		if (bg==FALSE) {
			while(!pid_chld)
				sigsuspend(&prev_mask);
		
			char env[8]; 
			snprintf(env, sizeof(env), "%d", return_value);
			/* save return value variable */
			setenv(q, env, 1);
				
			/* return shell to foreground */
			fg = shell_id;
			
			sleep(1);
			
			/* output info if job stopped */
			if(job_stopped==TRUE) {
				int jid;
				jid = get_id(pid_chld);
				job_status(pid_chld, 'S');
				printf("[%d]+ Stopped              %s\n", jid+1, job_list[jid].command);
				job_stopped=FALSE;
			}
			
			/* set to 0 in case of unexpected termination */
			pid_chld = 0;
		}
	}
		}
	if(exists) 
		/* if we reached here, the command was unidentifiable, print out error */
		fprintf(stderr, "%s: command not found\n", args[0]);
	}
    return 1;
}

/** executes built in commands **/
int builtin_command(char **argv) {
    /* if the new string = exit, then exit */
    if (!strcmp(argv[0], "exit")) {
		
        exit(EXIT_SUCCESS);
	}
    if (!strcmp(argv[0],"pwd")){
        /* Determines current path, stores it in path_ptr */
        long size = pathconf(".", _PC_PATH_MAX);
        char path_ptr[size];
        getcwd(path_ptr, (size_t)size);
        printf("%s\n", path_ptr);
        fflush(stdout);
        return 1;
    }
    if (!strcmp(argv[0],"cd")){
		if (argv[1]==NULL) {
			char* home = getenv("HOME");
			chdir(home);
		}
		else if (!strcmp(argv[1],"-")) {
			char* old = getenv("OLDPWD");
			chdir(old);
		}
		else if (!strcmp(argv[1],"..")) {
			char* above_dir = getenv("PWD");
			int length = strlen(above_dir);
			char buff[length];		
			strcpy(buff, above_dir);
			int i;
			for (i=length-1; i > 0; i--) {
				if (buff[i]=='/' && i != length-1) {
					buff[i]='\0';
					break;
				}
				buff[i]='\0';
			}
			char* str = "PWD";
			chdir(buff);
			setenv(str, buff, 1);
		}
		else {
			/* we are changing directories to next argument */
			int valid_dir = chdir(argv[1]);
			/* if the directory doesn't exist, print error message */
			if (valid_dir == -1)
				printf("-CSE320sh: cd: %s: %s\n", argv[1], strerror(ENOTDIR));
		}
		return(1);
    }
    if (!strcmp(argv[0],"echo")){
        int i;
        for (i=1; i < MAX_ARGS; i++){
            /* if NULL, reached end of line */
            if(argv[i]== NULL) {
                printf("\n");
                fflush(stdout);
                return 1;
            }
            /* if the input starts with $, it's a variable and needs to be grabbed 
               from printev */
            else if(*argv[i]=='$') {
                char* env_info = getenv(argv[i]+1);
                printf("%s", env_info);
                fflush(stdout);
            }
            /* else, just printing a word from command line */
            else
                printf("%s ",argv[i]);
                fflush(stdout);
        }
    }

	if (!strcmp(argv[0],"jobs")){
		int i, j;
		/* cycles through job list */
		for (j = 1; j <= JOB_TOTAL; j++) {
			for (i = 0; i < 10; i++) {
				if (job_list[i].id == j) {
					printf("[%d] (%d)", job_list[i].id, job_list[i].pgid);
					switch(job_list[i].state){
						case 'B': printf(" Running        ");
								  break;
						case 'S': printf(" Stopped        ");
						default:  break;
					} 
					printf("%s\n", job_list[i].command);
				}
			}
		}
		return 1;
	}
	if (!strcmp(argv[0],"kill")){
		
		if (argv[1]==NULL)
			return 1;
		
		int id;
		/* check to see if user supplied pid or job ID */
		id = config_id(argv[1]);
		if (id >0)
			Kill(id, SIGKILL);
		else
			printf("%s: No such job\n", argv[1]);
		return 1;
	}
	if (!strcmp(argv[0],"fg")){
		
		if (argv[1]==NULL)
			return 1;
		
		/* accept SIG_CHLD calls again */
		sigprocmask(SIG_SETMASK, &prev_mask, NULL);
			
		int id;
		/* check to see if user supplied pid or job ID */
		id = config_id(argv[1]);
		
		if (id >0 && id != shell_id) {
			/* continue in case it is stopped */
			Kill(id, SIGCONT);
			job_status(id, 'B');
			
			if (debug == TRUE) {
				int jid;
				jid = get_id(id);
                fprintf(stderr,"RUNNING: %s\n", job_list[jid].command);
			}
			
			/* put it in foreground */
			fg = id;
			
			pid_chld = 0;
				
			/* when child dies, it signals parent and changes
			 * pid_chld to 0 so the shell can continue and
			 * bg processes won't be zombies */
			 
			while(!pid_chld)
				sigsuspend(&prev_mask);	
				
			sleep(1); // race condition
			
					
			/* handle if user stops this code */
			if(job_stopped==TRUE) {
				int jid;
				job_status(pid_chld, 'S');
				jid = get_id(pid_chld);
				printf("[%d]+ Stopped              %s\n", jid+1, job_list[jid].command);
				job_stopped=FALSE;
			}
			
			char env[8]; 
			snprintf(env, sizeof(env), "%d", return_value);
			/* save return value variable */
			setenv(q, env, 1);
			
			fg = shell_id;
		}
		else
			printf("%s: No such job\n", argv[1]);
		return 1;
	}
	if (!strcmp(argv[0],"bg")){
		
		if (argv[1]==NULL)
			return 1;
		
		int id;
		/* check to see if user supplied pid or job ID */
		id = config_id(argv[1]);
		
		/* returns 0 if the id doesn't exist */
		if (id >0 && id != shell_id) {
			/* continue job */
			Kill(id, SIGCONT);
			
			if (debug == TRUE){
				int jid;
				jid = get_id(id);
                fprintf(stderr,"RUNNING: %s\n", job_list[jid].command);
			}
		}
		else
			printf("%s: No such job\n", argv[1]);
		return 1;
	}
	if (!strcmp(argv[0],"history")){

		int i;
		for(i = history_counter-1; i > history_counter-51; i--){
			if (history[i%50]==NULL)
				break;
			printf("%s", history[i%50]);
		}
		return 1;
	}
	if (!strcmp(argv[0],"clear-history")) {
		
		remove(history_filename);
		int i;
		
		for (i = 0; i < 50; i++) {
			char* add = (char*) history[i];
			*add = '\0';
		}
		
		history_counter=0;
		history_total=0;
		w_file = fopen(history_filename, "wd");
	
		return 1;
	}
	if (!strcmp(argv[0],"set")){
		char* space = "=";
		char set_variable[MAX_INPUT];
		if (argv[1] != NULL)
			strcpy(set_variable, argv[1]);
		else {
			printf("syntax error\n");
			return 1;
		}
		strcat(set_variable, space);
		if (argv[3] != NULL)
			strcat(set_variable, argv[3]);
		else {
			printf("syntax error\n");
			return 1;
		}
		putenv(set_variable);
		return 1;
	}
    return 0;
}

/** parses input arguments and returns whether or not the process is a background process **/
boolean parse_args(char* buf, char** args) {

    boolean ret = FALSE;
    char delim = ' ';
    int arg_total;
    /* This is to check our first strtok call */
    char* temp;

    /* remove endline character */
    buf[strlen(buf)-1] = ' '; 

    /* create arg list */
    arg_total = 0;
    
    /* Check first call to strtok */
    if((temp = strtok(buf, &delim)) == NULL){
        args[0] = "";
        return 0;
    }
    else{
        args[arg_total++] = temp;
    }
    
    /* Tokenize rest of argument */
    while ((temp = strtok(NULL, &delim)) != NULL) {
        /* Add tokenized value to our list if it is not null*/
        args[arg_total++] = temp;
        continue;
    }
    
    /* end the list */
    args[arg_total] = NULL;
	
	/* error checking with parse */
	if (strlen(args[0])<1 && (!(strcmp(args[0], ">")) || !(strcmp(args[0], "<"))
		 || !(strcmp(args[0], "|"))) && args[1]==NULL) {
		return ret;
	}
	
	int j;
	
    
    /* sets first process to the beginning of the list */
    process_list[0].args = args;
    process_list[0].argc = 0;
    
    /* Check for redirection */
    int i;

    boolean set_null = FALSE;
    for(i = 0; i < arg_total; i++){
        for(j = 0; args[i][j] != '\0'; j++){
            if(set_null == TRUE){
                args[i-1] = NULL;
                set_null = FALSE;
            }
            if(args[i][j] == '>'){
                /* Check if preceded by a 2 */
                if(j > 0){
                    if(args[i][j-1] == 2){
                       // process_list[total_processes].error = TRUE;
                    }
                }
                /* Redirect current process output to next process */
                redirection = TRUE;
                /* Set true for process run */
                process_list[total_processes].runf = TRUE;
                /* Create file descriptor for file being pointed to */
                int fd = open(args[i+1], O_RDWR | O_CREAT,  S_IRUSR | S_IWUSR);
                /* Add file descriptor to current process struct */
                process_list[total_processes].fdf = fd;
                
                /* Create new process */
                total_processes = total_processes + 1;
				process_list[total_processes].args = &args[i+1];
                /* Set list of process arguments to -1 (to be incremented post loop */
                process_list[total_processes].argc = -1;
                
                /* CHECK IF REDIRECT SYMBOL IS ALONE */
                /* Set redirect symbol to null */
                set_null = TRUE;
                //break;
                
            }
            else if(args[i][j] == '<'){
                /* Redirect next process output to current process */
                redirection = TRUE;
                /* Create new process */
                total_processes = total_processes + 1;
                process_list[total_processes].args = &args[i+1];
                process_list[total_processes].argc = -1;
                /* Set true for process run */
                process_list[total_processes].runp = TRUE;
                /* Create file descriptor for file being pointed to */
                int fd = open(args[i-1], O_RDWR | O_CREAT,  S_IRUSR | S_IWUSR);
                process_list[total_processes].fdp = fd;
                
                set_null = TRUE;
                
                
                
            }
            else if(args[i][j] == '|'){
                /* Piping is true */
                piping = TRUE;
                /* Create new process */
                total_processes = total_processes + 1;
                process_list[total_processes].args = &args[i+1];
                process_list[total_processes].argc = -1;
                process_list[total_processes].piped_to = TRUE;
                
                set_null = TRUE;
                
                
            }
            else if(args[i][j] == '&'){
					ret = TRUE;
					set_null = TRUE;
            }
        }
        
        process_list[total_processes].argc ++;
    }
    
    if(set_null == TRUE){
		/* double checks for SEG-FAULT */
		if ((i-1)>=0)
			args[i-1] = NULL;
    }

    return ret;
}

/** handles what happens when ctrl is pressed **/
void sig_handler(int sig)
{  
	if (fg!=shell_id)
		Kill(fg, sig);
	pid_chld=1;
}

/**  adds a job to job list **/
void create_job(pid_t id, char *command) 
{
	/* If process group id is negative, we have an error, 
	 * if it's 0, it's part of the kernel, so we definitely
	 * should not proceed */
    if (id < 1)
		return;

	int i;
	/* cycles through job list for a spot to add the job. */
	for (i = 0; i <= 128; i++) {
		/* no more room for jobs if all are full from 0-127, return */
		if (i==128) {
			printf("Error: too many jobs running.\n");
			return;
		}
		/* otherwise let's get in the job cannon and shoot ourselves to job-land 
		 * (where jobs grow on jobbies) */
		if (job_list[i].pgid == 0) {
			JOB_TOTAL++;
			job_list[i].state = 'B';
			job_list[i].id = i+1;
			job_list[i].pgid = id;
			strcpy(job_list[i].command, command);
			return;
		}
	}
}

/** deletes a job from the job list **/
void delete_job(int id) {
	int i;
	char* empty = "\0";
	
	/* looks for job by id */
	for (i = 0; i < 128; i++) {
		/* sets all to 0 */
		if (job_list[i].pgid == id) {
			job_list[i].id = 0;
			job_list[i].pgid = 0;
			job_list[i].state= '\0';

			if (debug == TRUE)
				fprintf(stderr,"ENDED: %s (ret = %d)\n", job_list[i].command, return_value);
			
			strcpy(job_list[i].command, empty);
			
			return;
		}
	}	
}

/** handles when a child process exits **/
void sig_child(int sig) {
	/* wait until received message of process exit or stop */
	int status;
	pid_chld = waitpid(-1, &status, WUNTRACED|WNOHANG);
	
	/* on ctrl+z, status returned is 5247 */
	if (status == 5247){
		job_stopped=TRUE;
	}
	else
		delete_job(pid_chld);
}

/** initialize job list **/
void init_jobs() {
	int i;
	char* empty = "\0";
	for (i = 0; i < 128; i++) {
		/* sets all to 0 */
		job_list[i].id = 0;
		job_list[i].pgid = 0;
		strcpy(job_list[i].command, empty);
		job_list[i].state= '\0';
	}	
}

/** get pid by job id **/
pid_t get_pid(int id) {
	int i;
    for (i = 0; i < 128; i++)
	if (job_list[i].id == id)
	    return job_list[i].pgid;
    return 0;
}

/** get job id by pid **/
pid_t get_id(int pid) {
	int i;
    for (i = 0; i < 128; i++)
	if (job_list[i].pgid == pid)
	    return i;
    return -1;
}

/** kill job wrapper **/
void Kill(pid_t pid, int sig) {
	if (kill(pid, sig) < 0)
		printf("Error terminating job.\n");
	if (sig==SIGINT) {
		kill(pid, SIGTERM); 
		delete_job(pid);
	}
}

/** update a jobs status **/
void job_status(pid_t pid, char state) {
	int i;
	for (i = 0; i < 128; i++) {
		if (job_list[i].pgid == pid) {
			job_list[i].state = state;
			break;
		}
	}
}
/** return the PID of a job depending on how the user specified for it */
int config_id(char* buf) {
	int id;
	if (*buf == '%') {
		/* job id */
		id = atoi(buf + 1);
		
		if (job_list[id-1].pgid==0)
			id = -1;
		else
			/* get the associated pgid */
			id = get_pid(id);
	}
	else {
		/* pid */
		id = atoi(buf);
		
		int valid = get_id(id);
		
		/* check to see if there is a job associated with this pid */
		if (valid==-1)
			id = -1;
	}
		
	return id;
}

void get_filename(char** args) {
	char* path_list = getenv("PATH");
	long size = pathconf(".", _PC_PATH_MAX);
    char path_ptr[size];
    getcwd(path_ptr, (size_t)size);

    struct stat buffer;
    int exists=1;

    if(*args[0] == '.') 
    { 
            /* the user is trying to run from the current directory */
            strcpy(filename, path_ptr);
            strcat(filename,(args[0]+1));
    }
    else if(*args[0] == '/') 
            /* the user is trying to run from another directory */
            strcpy(filename, args[0]);
    else {
            /* the user wants us to search through each path directory */
            char* path_ptr = malloc(strlen(path_list));
            strcpy(path_ptr, path_list);
            char* path;
            char* place_holder = path_ptr;
            /* space is malloced because str_tok is a pain in the ass (lol) */
            while(exists) {
                path = strtok(path_ptr, ":");
                if (path==NULL)
                    break;
                strcpy(filename, path);
                strcat(filename, "/");
                strcat(filename, args[0]);
                exists = stat(filename, &buffer);
                path_ptr = NULL;
        }
        free(place_holder);
    }

}
